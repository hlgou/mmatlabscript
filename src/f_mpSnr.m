% ------------------------------------------------------------------
% FileName:         f_mpSnr.m
% Author:           hlgou
% Version:          V 1.0
% Created:          2021-10-09
% Description:      read, process, draw mp_snr data
%   Detil:          https://blog.csdn.net/Gou_Hailong/article/details/115092884
%   want:           Adaptive adjustment of satellite PRN
% History:
%          2021-10-09   hlgou       :Create the file
%          2021-xx-xx   xxxxxxxx    :Do some modified
% ------------------------------------------------------------------

% clear;clc;
% datadir = 'C:\Users\OHanlon\Desktop\plotData\mp_snr\';
% sys = 'E';
% [result, lgs, satlist] = getResult(datadir, sys);
% sat = 'E11';
% pgcf2=draw_MPSNR_time_all(result,satlist,sat);
% gcf=pgcf2;
% subplot(2,1,1);
% hl=legend('E1','E5a','E5b','E6','E5');
% set(hl,'Orientation','horizon');
% set(hl,'Box','off');

% ------------------------------------------------------------------

clear;clc;
whsp = 'C:\Users\OHanlon\Desktop\lv\snr\whsp-';
xgxn = 'C:\Users\OHanlon\Desktop\lv\snr\xgxn-';
sys = 'C';
freqlist=['L2';'L7'];

[pResult, lgs, satlist] = getResult1(whsp, sys);
[nResult, lgs, satlist] = getResult1(xgxn, sys);



% f1=[whsp,'-',sys,'L1.txt'];
% f2=[xgxn,'-',sys,'L1.txt'];
% num = getlist(sys);
% satlist = num2sat_char(sys, num);


sat = 'C00';
pgcf2=draw_MPSNR_time_all_sub(pResult,nResult,satlist,sat);
gcf=pgcf2;
subplot(2,1,1);
hl=legend(lgs);
set(hl,'Orientation','horizon');
set(hl,'Box','off');

% main draw to draw fixed files
function draw(result1, result2, satlist1, satlist2, lgs1, lgs2)
    sys = 'G'; site1 = [sys, '-NNOR-2020110'];
    sys = 'E'; site2 = [sys, '-NNOR-2020110'];
    figure
    wid = 800;
    hei = 400;
    bili = wid / hei;
    wid1 = 0.43;
    lefx = 1 - wid1 * 2;
    lefy = lefx * bili;
    hei1 = (1 - lefy) / 2;
    magx = lefx * 8/14; %margin   x
    intx = lefx * 3/14; %interval x
    magy = lefy * 8/14; %margin   y
    inty = lefy * 3/14; %interval y
    magx1 = magx + wid1 + intx;
    magy1 = magy + hei1 + inty;
    x = 10:20:90; x1 = sprintfc('%g', x); xl = [10 90];
    x2 = {'', '', '', '', ''};
    s = 30:10:70; s1 = sprintfc('%g', s); yl1 = [30 70];
    s2 = {'', '', '', '', ''};
    m = -1:1; m2 = {'', '', ''}; m1 = sprintfc('%g', m); yl2 = [-1.5 1.5];

    set(gcf, 'position', [0 0 wid hei])
    subplot(221)
    set(gca, 'position', [magx magy1 wid1 hei1], 'xtick', [], 'ytick', [], 'box', 'on')
    draw_MPSNR(result1, satlist1, 1);
    title(site1, 'FontSize', 10, 'FontName', 'Arial', 'FontWeight', 'normal');
    ylabel('SNR(dBHz)', 'FontSize', 10);
    hl = legend(lgs1);
    set(hl, 'Orientation', 'horizon');
    set(hl, 'Box', 'off');
    title(site1, 'FontSize', 10, 'FontName', 'Arial', 'FontWeight', 'normal');
    set(gca, 'YLim', yl1, 'YTick', s, 'YTicklabel', s1);
    set(gca, 'XLim', xl, 'XTick', x, 'XTicklabel', x2);

    subplot(222)
    set(gca, 'position', [magx1 magy1 wid1 hei1], 'xtick', [], 'ytick', [], 'box', 'on')
    draw_MPSNR(result2, satlist2, 1);
    title(site2, 'FontSize', 10, 'FontName', 'Arial', 'FontWeight', 'normal');
    hl = legend(lgs2);
    set(hl, 'Orientation', 'horizon');
    set(hl, 'Box', 'off');
    title(site2, 'FontSize', 10, 'FontName', 'Arial', 'FontWeight', 'normal');
    set(gca, 'YLim', yl1, 'YTick', s, 'YTicklabel', s2);
    set(gca, 'XLim', xl, 'XTick', x, 'XTicklabel', x2);

    subplot(223)
    set(gca, 'position', [magx magy wid1 hei1], 'xtick', [], 'ytick', [], 'box', 'on')
    draw_MPSNR(result1, satlist1, 2);
    set(gca, 'YLim', yl2, 'YTick', m, 'YTicklabel', m1);
    set(gca, 'XLim', xl, 'XTick', x, 'XTicklabel', x1);
    xlabel('Elevation(°)', 'FontSize', 10);
    ylabel('Multipath(m)', 'FontSize', 10);

    subplot(224)
    set(gca, 'position', [magx1 magy wid1 hei1], 'xtick', [], 'ytick', [], 'box', 'on')
    draw_MPSNR(result2, satlist2, 2);
    set(gca, 'YLim', yl2, 'YTick', m, 'YTicklabel', m2);
    set(gca, 'XLim', xl, 'XTick', x, 'XTicklabel', x1);
    xlabel('Elevation(°)', 'FontSize', 10);
end


function [result, lgs, satlist] = getResult(datadir, sys)
% read file from files which the name is fixed, Attention!
%
% > @param[in] datadir:     the path of data
% > @param[in] sys:         the single system which you want data
% return:
% < @param[out] result:     the container for storing data
% < @param[out] lgs:        the legends of the figure(the signal name)
% < @param[out] satlist:    the char-satlist
    if sys == 'G'
        str = ['GL1'; 'GL2'; 'GL5'];
        lgs = {'L1'; 'L2'; 'L5'};
        all = [1:32]; lack = [23];
        num = msubmat(all, lack);
    elseif sys == 'E'
        str = ['EL1'; 'EL5'; 'EL7'; 'EL6'; 'EL8'];
        lgs = {'E1'; 'E5a'; 'E5b'; 'E6'; 'E5'};
        all = [1:36]; lack = [6 10 16 17 20 22 23 28 29 32 34 35];
        num = msubmat(all, lack);
    end
    satlist = num2sat_char(sys, num);
    nf = size(str, 1); n = size(satlist, 1);
    fname = [];
    for i = 1:nf
        tmp = [datadir, str(i, :), '.txt'];
        Data = MP_SNR2data(tmp, satlist);
        eval(['Data', num2str(i), '=Data;']);
    end
    for k = 1:n
        for f = 1:nf
            eval(['result(k).B', num2str(f), '=Data', num2str(f), '(k).B1;']);
        end
    end
end


function [result, lgs, satlist] = getResult1(datadir, sys)
    if sys == 'G'
        str = ['GL1'; 'GL2'];
        lgs = {'L1'; 'L2'};
        all = [1:32]; lack = [23];
        num = msubmat(all, lack);
    elseif sys == 'E'
        str = ['EL1';'EL7'];
        lgs = {'E1'; 'E5b'};
        all = [1:36]; lack = [6 10 16 17 20 22 23 28 29 32 34 35];
        num = msubmat(all, lack);
    elseif sys == 'C' 
        str = ['CL2';'CL7'];
        lgs = {'B1I'; 'B2I'};
        all=[1:60];lack=[31,56:58];
        num = msubmat(all, lack);
    end
    satlist = num2sat_char(sys, num);
    nf = size(str, 1); n = size(satlist, 1);
    fname = [];
    for i = 1:nf
        tmp = [datadir, str(i, :), '.txt'];
        Data = MP_SNR2data(tmp, satlist);
        eval(['Data', num2str(i), '=Data;']);
    end
    for k = 1:n
        for f = 1:nf
            eval(['result(k).B', num2str(f), '=Data', num2str(f), '(k).B1;']);
        end
    end
end


function pgcf = draw_MPSNR_time(result, satlist, sat, who)
% draw snr or mp in one picture | xlable is time
%
% > @param[in] result:      the container for storing data
% < @param[in] satlist:     the char-satlist
% < @param[in] sat:         the satellite PRN which you want to plot, "X00" stand for all
% > @param[in] who:         the mode which you want to plot, "mp"=mulit path, others is snr
% return:
% < @param[out] pgcf:       the handle of the figure which you can save the figure by this
    n = size(satlist, 1); %number of sat
    b = length(fieldnames(result)); %number of band
    crl_ = ['rbgycmk']; %color
    crl = crl_(1:b); crl = fliplr(crl);
    prn = 0;
    for i = 1:n
        if (satlist(i, :) == sat)
            prn = i;
            break;
        end
    end
    if prn == 0
        for j = 1:b
            str = ['Data', num2str(j)];
            eval([str, '=[];']);
            for i = 1:n
                str1 = ['result(', num2str(i), ')'];
                eval([str, '=[', str, ';', str1, '.B', num2str(j), '];']);
            end
        end
    else
        for i = 1:b
            eval(['Data', num2str(i), '=result(prn).B', num2str(i), ';']);
        end
    end
    figure
    if who == 'mp'
        for i = 1:b
            col = crl(i);
            str = ['Data', num2str(i)];
            eval(['plot(', str, '(:,1),', str, '(:,4),', '''', eval('col'), '.'');']); %mp
            hold on;
        end
        ylabel('Multipath(m)', 'FontSize', 10);
    else
        for i = 1:b
            col = crl(i);
            str = ['Data', num2str(i)];
            eval(['plot(', str, '(:,1),', str, '(:,3),', '''', eval('col'), '.'');']); %sn
            hold on;
        end
        ylabel('SNR(dBHz)', 'FontSize', 10);
    end

    xlabel('GPS Time(hour)', 'FontSize', 10);
    set(gca, 'XLim', [0, 86400]);
    set(gca, 'XTick', 0:21600:86400);
    set(gca, 'xticklabel', {'0', '6', '12', '18', '24'});
    set(gca, 'FontSize', 10, 'FontName', 'Arial');
    pgcf = gcf;
end


function pgcf = draw_MPSNR_time_all(result, satlist, sat)
% draw snr or mp in one picture | xlable is time
%
% > @param[in] result:      the container for storing data
% < @param[in] satlist:     the char-satlist
% < @param[in] sat:         the satellite PRN which you want to plot, "X00" stand for all
% return:
% < @param[out] pgcf:       the handle of the figure which you can save the figure by this
    n = size(satlist, 1); %number of sat
    b = length(fieldnames(result)); %number of band
    crl_ = ['rbgycmk']; %color
    crl = crl_(1:b); crl = fliplr(crl);

    prn = 0;
    for i = 1:n
        if (satlist(i, :) == sat)
            prn = i;
            break;
        end
    end
    if prn == 0
        prn = 1:n;      % all satellites
    end
    for j = 1:b
        str = ['Data', num2str(j)];
        eval([str, '=[];']);
        for i = prn
            str1 = ['result(', num2str(i), ')'];
            eval([str, '=[', str, ';', str1, '.B', num2str(j), '];']);
        end
    end
    figure
    for i = 1:b
        col = crl(i);
        str = ['Data', num2str(i)];
        subplot(2, 1, 1);
        eval(['plot(', str, '(:,1),', str, '(:,3),', '''', eval('col'), '.'');']); %snr
        hold on;
        subplot(2, 1, 2);
        eval(['plot(', str, '(:,1),', str, '(:,4),', '''', eval('col'), '.'');']); %mp
        hold on;
    end
    set(gca, 'FontSize', 10, 'FontName', 'Arial');
    subplot(2, 1, 1);
    set(gca, 'XLim', [0, 86400]);
    set(gca, 'XTick', 0:21600:86400);
    set(gca, 'xticklabel', {'0', '6', '12', '18', '24'});
    ylabel('SNR(dBHz)', 'FontSize', 10);
    set(gca, 'YLim', [30, 70], 'YTick', 30:10:70);
    subplot(2, 1, 2);
    ylabel('Multipath(m)', 'FontSize', 10);
    set(gca, 'YLim', [-1.5, 1.5], 'YTick', -1:1);
    set(gca, 'XLim', [0, 86400]);
    set(gca, 'XTick', 0:21600:86400);
    set(gca, 'xticklabel', {'0', '6', '12', '18', '24'});
    set(gca, 'FontSize', 10, 'FontName', 'Arial');
    xlabel('GPS Time(hour)', 'FontSize', 10);
    pgcf = gcf;
end


function pgcf = draw_MPSNR_time_all_sub(result, result2, satlist, sat)
% draw snr or mp in one picture | xlable is time
%
% > @param[in] result:      the container for storing data
% < @param[in] satlist:     the char-satlist
% < @param[in] sat:         the satellite PRN which you want to plot, "X00" stand for all
% return:
% < @param[out] pgcf:       the handle of the figure which you can save the figure by this
    n = size(satlist, 1); %number of sat
    b = length(fieldnames(result)); %number of band
    crl_ = ['rbgycmk']; %color
    crl = crl_(1:b); crl = fliplr(crl);

    prn = 0;
    for i = 1:n
        if (satlist(i, :) == sat)
            prn = i;
            break;
        end
    end
    if prn == 0
        prn = 1:n;      % all satellites
    end
    for j = 1:b
        str = ['Data', num2str(j)];
        eval([str, '=[];']);
        for i = prn
            str1 = ['result(', num2str(i), ')'];
            str2 = ['result2(', num2str(i), ')'];
            fprintf(['band',num2str(j),', prn-',num2str(i),':  ']);
            eval(['tmp=subdata(',str1, '.B', num2str(j),',',str2,'.B', num2str(j),');']);
            if ~isempty(tmp) && max(abs(tmp(:,5)))>5      % here rm sat_big
                continue;
            end
            eval([str, '=[', str, ';tmp];']);
        end
    end
    
    disp('L1:')
    fprintf('Mean(delt_snr)=%f,Std(delt_snr)=%f\n',mean(Data1(:,6)),std(Data1(:,6)));
    fprintf('Mean(delt_ele)=%f,Std(delt_ele)=%f\n',mean(Data1(:,5)),std(Data1(:,5)));
    disp('L2:')
    fprintf('Mean(delt_snr)=%f,Std(delt_snr)=%f\n',mean(Data2(:,6)),std(Data2(:,6)));
    fprintf('Mean(delt_ele)=%f,Std(delt_ele)=%f\n',mean(Data2(:,5)),std(Data2(:,5)));
    figure
    subplot(2,1,1)
    hist(Data1(:,6))
    title('L1-\Delta SNR')
    subplot(2,1,2)
    hist(Data1(:,5))
    title('\Delta ELE')
    
    figure
    subplot(2,1,1)
    hist(Data2(:,6))
    title('L2-\Delta SNR')
    subplot(2,1,2)
    hist(Data2(:,5))
    title('\Delta ELE')
    figure
    for i = 1:b
        col = crl(i);
        str = ['Data', num2str(i)];
        subplot(2, 1, 1);
        eval(['plot(', str, '(:,1),', str, '(:,5),', '''', eval('col'), '.'');']); %snr
        hold on;
        subplot(2, 1, 2);
        eval(['plot(', str, '(:,5),', str, '(:,6),', '''', eval('col'), '.'');']); %mp
        hold on;
    end
    set(gca, 'FontSize', 10, 'FontName', 'Arial');
    subplot(2, 1, 1);
%     set(gca, 'XLim', [0, 86400]);
%     set(gca, 'XTick', 0:21600:86400);
%     set(gca, 'xticklabel', {'0', '6', '12', '18', '24'});
    ylabel('SNR(dBHz)', 'FontSize', 10);
    %set(gca, 'YLim', [30, 70], 'YTick', 30:10:70);
    subplot(2, 1, 2);
    ylabel('Multipath(m)', 'FontSize', 10);
    %set(gca, 'YLim', [-1.5, 1.5], 'YTick', -1:1);
%     set(gca, 'XLim', [0, 86400]);
%     set(gca, 'XTick', 0:21600:86400);
%     set(gca, 'xticklabel', {'0', '6', '12', '18', '24'});
    set(gca, 'FontSize', 10, 'FontName', 'Arial');
    xlabel('GPS Time(hour)', 'FontSize', 10);
    pgcf = gcf;
    
    
end


function pgcf = draw_MPSNR_elv(result, satlist, sat, who)
% draw snr or mp in one picture | xlable is elv
%
% > @param[in] result:      the container for storing data
% < @param[in] satlist:     the char-satlist
% < @param[in] sat:         the satellite PRN which you want to plot, "X00" stand for all
% > @param[in] who:         the mode which you want to plot, "mp"=mulit path, others is snr
% return:
% < @param[out] pgcf:       the handle of the figure which you can save the figure by this
    n = size(satlist, 1); %number of sat
    b = length(fieldnames(result)); %number of band
    crl_ = ['rbgycmk']; %color
    crl = crl_(1:b); crl = fliplr(crl);
    prn = 0;
    for i = 1:n
        if (satlist(i, :) == sat)
            prn = i;
            break;
        end
    end
    if prn == 0
        for j = 1:b
            str = ['Data', num2str(j)];
            eval([str, '=[];']);
            for i = 1:n
                str1 = ['result(', num2str(i), ')'];
                eval([str, '=[', str, ';', str1, '.B', num2str(j), '];']);
            end
        end
    else
        for i = 1:b
            eval(['Data', num2str(i), '=result(prn).B', num2str(i), ';']);
        end
    end

    figure
    if who == 'mp'
        for i = 1:b
            col = crl(i);
            str = ['Data', num2str(i)];
            eval(['plot(', str, '(:,2),', str, '(:,4),', '''', eval('col'), '.'');']); %mp
            hold on;
        end
        ylabel('Multipath(m)', 'FontSize', 10);
    else
        for i = 1:b
            col = crl(i);
            str = ['Data', num2str(i)];
            eval(['plot(', str, '(:,2),', str, '(:,3),', '''', eval('col'), '.'');']); %sn
            hold on;
        end
        ylabel('SNR(dBHz)', 'FontSize', 10);
    end
    xlabel('Elevation(°)', 'FontSize', 10);
    set(gca, 'FontSize', 10, 'FontName', 'Arial');
    pgcf = gcf;
end


function pgcf = draw_MPSNR_elv_all(result, satlist, sat)
% draw snr and mp in one picture | xlable is elv
%
% > @param[in] result:      the container for storing data
% < @param[in] satlist:     the char-satlist
% < @param[in] sat:         the satellite PRN which you want to plot, "X00" stand for all
% return:
% < @param[out] pgcf:       the handle of the figure which you can save the figure by this
    n = size(satlist, 1); %number of sat
    b = length(fieldnames(result)); %number of band
    crl_ = ['rbgycmk']; %color
    crl = crl_(1:b); crl = fliplr(crl);

    prn = 0;
    for i = 1:n
        if (satlist(i, :) == sat)
            prn = i;
            break;
        end
    end
    if prn == 0
        prn = 1:n;      % all satellites
    end
    for j = 1:b
        str = ['Data', num2str(j)];
        eval([str, '=[];']);
        for i = prn
            str1 = ['result(', num2str(i), ')'];
            eval([str, '=[', str, ';', str1, '.B', num2str(j), '];']);
        end
    end
    figure
    for i = 1:b
        col = crl(i);
        str = ['Data', num2str(i)];
        subplot(2, 1, 1);
        eval(['plot(', str, '(:,2),', str, '(:,3),', '''', eval('col'), '.'');']); %snr
        hold on;
        subplot(2, 1, 2);
        eval(['plot(', str, '(:,2),', str, '(:,4),', '''', eval('col'), '.'');']); %mp
        hold on;
    end
    subplot(2, 1, 1);
    ylabel('SNR(dBHz)', 'FontSize', 10);
    set(gca, 'YLim', [30, 70], 'YTick', 30:10:70);
    subplot(2, 1, 2);
    ylabel('Multipath(m)', 'FontSize', 10);
    set(gca, 'YLim', [-1.5, 1.5], 'YTick', -1:1);
    xlabel('Elevation(°)', 'FontSize', 10);
    set(gca, 'FontSize', 10, 'FontName', 'Arial');
    pgcf = gcf;
end


function draw_MPSNR(result, satlist, g)
% draw snr or mp in one picture
%
% > @param[in] result:      the container for storing data
% < @param[in] satlist:     the char-satlist
% < @param[in] g:           g=1 stand for snr, g=others stand for mp
    n = size(satlist, 1); %number of sat
    b = length(fieldnames(result)); %number of band
    crl_ = ['rbgycmk']; %color
    crl = crl_(1:b); crl = fliplr(crl);
    for j = 1:b
        str = ['Data', num2str(j)];
        eval([str, '=[];']);
        for i = 1:n
            str1 = ['result(', num2str(i), ')'];
            eval([str, '=[', str, ';', str1, '.B', num2str(j), '];']);
        end
    end

    for i = 1:b
        col = crl(i);
        str = ['Data', num2str(i)];
        if g == 1
            eval(['plot(', str, '(:,2),', str, '(:,3),', '''', eval('col'), '.'');']); %snr
        else
            eval(['plot(', str, '(:,2),', str, '(:,4),', '''', eval('col'), '.'');']); %mp
        end
        hold on;
    end
end


function result = MP_SNR2data(file1, satlist)
% read MP and SNR data from txt(from rtklib)
%
% > @param[in] file1:       the file name of the data
% < @param[in] satlist:     the char-satlist
    fid1 = fopen(file1, 'r');
    n = size(satlist, 1);
    for k = 1:n
        result(k).B1 = [];
    end
    while ~feof(fid1)
        tline = fgetl(fid1);
        tline = strtrim(tline);
        if (tline(1) ~= '%')
            tmp = regexp(tline, '\s+', 'split');
            for i = 1:n
                if (satlist(i, :) == tmp{3})
                    prn = i;
                    break;
                end
            end
            elev = str2num(char(tmp(5)));
            if elev < 10
                continue;
            end
            atime = regexp(tmp{2}, '\:', 'split');
            time = str2num(atime{1}) * 3600 + str2num(atime{2}) * 60 + str2num(atime{3});
            snr = str2num(char(tmp(6)));
            mp = str2num(char(tmp(7)));
            epoch_result = [time, elev, snr, mp];
            result(prn).B1 = [result(prn).B1; epoch_result];
        end
    end
    fclose(fid1);
end


function satlist = num2sat_char(sys, num)
% Util: sat_num list to sat_char list
    %this function can trans numList to charSatList
    satlist = [];
    for i = num
        satlist = [satlist; sys, num2str(i, '%02d')];
    end
end


function data = msubmat(all, lack)
% Util: rm bad sat from num_sat list
    %this function can achieve sub B from A
    n = length(lack);
    for i = 1:n
        all(find(all == lack(i))) = [];
    end
    data = all;
end


function num = getlist(sys)
    if sys == 'G'
        all = [1:32]; lack = [23];
    elseif sys == 'E'
        all = [1:36]; lack = [6 10 16 17 20 22 23 28 29 32 34 35];
    elseif sys == 'C'
        all=[1:60];lack=[31,56:58];
    num=msubmat(all,lack);
    end
end


function sub = subdata(Data1,Data2)
if isempty(Data1) || isempty(Data2)
    sub=[];
    return
end
A=Data1(:,1);
B=Data2(:,1);
C=intersect(A, B);
sub=zeros(size(C,1),7);
sub(:,1:4)=Data2(ismember(C,B)==1,:);
sub(:,5:end)=Data1(ismember(C,A)==1,2:end)-Data2(ismember(C,B)==1,2:end);
fprintf('Mean(delt_ele)=%f,Std(delt_ele)=%f  ',mean(sub(:,5)),std(sub(:,5)));
fprintf('Mean(delt_snr)=%f,Std(delt_snr)=%f\n',mean(sub(:,6)),std(sub(:,6)));
end

