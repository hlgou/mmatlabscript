% ------------------------------------------------------------------
% FileName:         f_satNumRec.m
% Author:           hlgou
% Version:          V 1.0
% Created:          2022-01-13
% Description:      plot the number of the sats
% History:
%          2022-01-13   hlgou       :Create the file
%          2021-xx-xx   xxxxxxxx    :Do some modified
% ------------------------------------------------------------------

clear,clc;
wdir = 'D:\navigation\gnssData\prod\2195\satnums\';
data1 = importdata([wdir,'gbm21955_C2.txt']);
data2 = importdata([wdir,'gbm21955_C3.txt']);
data3 = data2;
data3(:,3) = data2(:,3) + data1(:,3);

data4 = arrange_data(data3); % 1:BDS2, 2:BDS3, 3:BDS3+BDS2
[Plg,Plt] = meshgrid(-177.5:5:177.5,88.75:-2.5:-88.75);
m_proj('Equidistant cylindrical','lon',[-180,180],'lat',[-90,90]); 
m_pcolor(Plg,Plt,data4);
shading interp;
hold on;
m_coast;
% m_grid('xtick',-120:60:120,'xticklabel','','yticklabel','');
m_grid('box','fancy','xtick',-180:60:180,'ytick',-90:30:90);
set(gca,'ytick',-90:30:90);
a=jet;
colormap(a(60:end,:));colorbar;caxis([0,30]);  % set the the value range of colorbar
title('BDS-2+BDS-3','FontWeight','normal');
% colorbar('location','east')

function data4 = arrange_data(data2)
% Change data arrangement
    for i=-177.5:5:177.5
        for j=-88.75:2.5:88.75
            k=find(data2(:,1)==i&data2(:,2)==j);
            a=(j+88.75)/2.5+1;
            b=(i+177.5)/5+1;
            data4(a,b)=data2(k,3);
        end
    end
end