% ------------------------------------------------------------------
% FileName:         f_time.m
% Author:           hlgou
% Version:          V 1.0
% Created:          2021-10-09
% Description:      time convert
% History:
%          2021-10-09   hlgou       :Create the file
%          2021-xx-xx   xxxxxxxx    :Do some modified
% ------------------------------------------------------------------

function A = f_time
    A.leapyear = @leapyear;
    A.norm_doy = @norm_doy;
    A.doy2ymd = @doy2ymd;
    A.ymd2doy = @ymd2doy;
    A.ymd2mjd = @ymd2mjd;
    A.doy2mjd = @doy2mjd;
    A.ymd2gpsweek = @ymd2gpsweek;
    A.mjd2ydoy = @mjd2ydoy;
    A.sod2hms = @sod2hms;
    A.hms2sod = @hms2sod;
    A.sod2how = @sod2how;
end

% Determine if a year is a leap year
function whe = leapyear(year)
    whe = 0;
    if rem(year, 4) == 0 && rem(year, 100) ~= 0
        whe = 1;
    end
    if rem(year, 400) == 0
        whe = 1;
    end
end

% convert an illegal doy to a normal doy
function [year, doy] = norm_doy(year, doy)
    while (1)
        if doy <= 0
            year = year - 1;
            doy = doy + 365 + leapyear(year);
        elseif doy >= 365 + leapyear(year)
            doy = doy - (365 - leapyear(year));
            year = year + 1;
        else
            break;
        end
    end
end

% Change year, day of year to year, month, day
function [year, mon, day] = doy2ymd(year, doy)
    day = doy;
    mon = 0;
    monthdays = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    for i = 1:13
        monthday = monthdays(i);
        if i == 3 && leapyear(year) == 1
            monthday = monthday + 1;
        end
        if day > monthday
            day = day - monthday;
        else
            mon = i - 1;
            break
        end
    end
end

% Change year, month, day to year, day of year
function [year, doy] = ymd2doy(year, mon, day)
    doy = day;
    monthdays = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    for i = 1:mon
        doy = doy + monthdays(i);
    end
    if mon > 2
        doy = doy + leapyear(year);
    end
end

% Change year, month, day to Modified Julian Day
function mjd = ymd2mjd(year, mon, day)
    if mon <= 2
        mon = mon + 12;
        year = year - 1;
    end
    mjd = 365.25 * year - rem(365.25 * year, 1.0) - 679006.0;
    mjd = mjd + floor(30.6001 * (mon + 1)) + 2.0 - ...
        floor(year / 100.0) + floor(year / 400) + day;
end

% Change year, day of year to Modified Julian Day
function mjd = doy2mjd(year, doy)
    [year, doy] = norm_doy(year, doy);
    [year, mon, day] = doy2ymd(year, doy);
    mjd = ymd2mjd(year, mon, day);
end

% Change year,month,day to GPS weeks
function [week, day] = ymd2gpsweek(year, mon, day)
    mjd = ymd2mjd(year, mon, day);
    week = floor((mjd - 44244.0) / 7.0);
    day = floor(mjd - 44244.0 - week * 7.0);
end

% Change Modified Julian Day to year, day of year
% The input date must after 1952.0
function [year, doy] = mjd2ydoy(mjd)
    year = 1952;
    dd = mjd + 1 - 34012;
    yday = 366;
    while (dd > yday)
        dd = dd - yday;
        year = year + 1;
        yday = 365 + leapyear(year);
    end
    year = floor(year);
    doy = floor(dd);
end

% Change seconds of day to hours, minutes and seconds
function [hh, mm, ss] = sod2hms(sod)
    hh = floor(sod / 3600);
    mm = floor((sod - hh * 3600) / 60);
    ss = floor(sod - hh * 3600 - mm * 60);
end

% Change hours:minutes:seconds to seconds of day
function sod = hms2sod(hh, mm, ss)
    sod = floor(hh) * 3600 + floor(mm) * 60 + ss;
end

% Change (Modified Julian Day, seconds fo day) to (GPS week, hours of week)
function [week, how] = sod2how(mjd, sod)
    [year, doy] = mjd2ydoy(mjd);
    [year, mon, day] = doy2ymd(year, doy);
    [week, day] = ymd2gpsweek(year, mon, day);
    how = (sod + day * 86400) / 3600.0;
end

%%
