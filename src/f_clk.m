% ------------------------------------------------------------------
% FileName:         f_clk.m
% Author:           hlgou
% Version:          V 1.0
% Created:          2021-10-09
% Description:      read, process, draw clk-files such as great-clk, PIFCB
% History:
%          2021-10-09   hlgou       :Create the file
%          2021-xx-xx   xxxxxxxx    :Do some modified
% ------------------------------------------------------------------

function A = f_clk
    A.read_clk12 = @read_clk12;
    A.read_clk13 = @read_clk13;
    A.read_igs_clk = @read_igs_clk;
    A.read_ifcb = @read_ifcb;
    A.draw_clkdif = @draw_clkdif;
    A.draw_ifcb = @draw_ifcb;
    A.zhongxinh = @zhongxinh;
    A.zhongxinl = @zhongxinl;
end

% read clk12 product from GREAT
function Data1 = read_clk12(sysid, interval, file1)
    %   eg: Data1=read_clk12('G',300,file1)
    switch sysid
        case 'G'
            lgds = ['G01'; 'G02'; 'G03'; 'G04'; 'G05'; 'G06'; 'G07'; 'G08'; 'G09'; 'G10'; 'G11'; 'G12';
                'G13'; 'G14'; 'G15'; 'G16'; 'G17'; 'G18'; 'G19'; 'G20'; 'G21'; 'G22'; 'G23'; 'G24';
                'G25'; 'G26'; 'G27'; 'G28'; 'G29'; 'G30'; 'G31'; 'G32'];
        case 'E'
            lgds = ['E01'; 'E02'; 'E03'; 'E04'; 'E05'; 'E07'; 'E08'; 'E09'; 'E11'; 'E12'; 'E13'; 'E14';
                'E15'; 'E18'; 'E19'; 'E21'; 'E24'; 'E25'; 'E26'; 'E27'; 'E30'; 'E31'; 'E33'; 'E36'];
        case 'C'
            lgds = ['C06'; 'C07'; 'C08'; 'C09'; 'C10'; 'C11'; 'C12'; 'C13'; 'C14'; 'C16'];

        case 'R'
            lgds = ['R01'; 'R02'; 'R03'; 'R04'; 'R05'; 'R07'; 'R08'; 'R09'; 'R10'; 'R11'; 'R13'; 'R14';
                'R15'; 'R16'; 'R17'; 'R18'; 'R19'; 'R20'; 'R21'; 'R22'; 'R23'; 'R24'; ];
        case 'J'
            lgds = ['J01'; 'J02'; 'J03'];

    end
    nsat = size(lgds, 1);
    len = 86400 / interval;
    fid1 = fopen(file1, 'r');
    Data1 = zeros(nsat, len);
    while ~feof(fid1)
        line = fgetl(fid1);
        if (line(1:2) == 'AS')
            sec = (str2num(line(19:21)) * 3600 + str2num(line(22:24)) * 60 + str2num(line(25:34))) / interval;
            if sec ~= fix(sec)
                continue;
            end
            prn = line(4:6);
            for i = 1:nsat
                if (lgds(i, :) == prn)
                    time = sec + 1;
                    Data1(i, time) = str2num(line(41:59));
                    break;
                end
            end
        end
    end
    fclose(fid1);
end

% read clk13 product from GREAT
function Data1 = read_clk13(sysid, interval, file1)
    %   eg: Data1=read_clk13('G',300,file1)
    switch sysid
        case 'G'
            lgds = ['G01'; 'G03'; 'G06'; 'G08'; 'G09'; 'G10'; 'G24'; 'G25'; 'G26'; 'G27'; 'G30'; 'G32'];
        case 'E'
            lgds = ['E01'; 'E02'; 'E03'; 'E04'; 'E05'; 'E07'; 'E08'; 'E09'; 'E11'; 'E12'; 'E13'; 'E14'; 'E15'; 'E18'; 'E19'; 'E21'; 'E24'; 'E25'; 'E26'; 'E27'; 'E30'; 'E31'; 'E33'; 'E36'];
        case 'C'
            lgds = ['C06'; 'C07'; 'C08'; 'C09'; 'C10'; 'C11'; 'C12'; 'C13'; 'C14'; 'C16'];
    end
    nsat = size(lgds, 1);
    len = 86400 / interval;
    fid1 = fopen(file1, 'r');
    Data1 = zeros(nsat, len);
    while ~feof(fid1)
        line = fgetl(fid1);
        if (line(1:2) == 'AS')
            sec = (str2num(line(19:21)) * 3600 + str2num(line(22:24)) * 60 + str2num(line(25:34))) / interval;
            if sec ~= fix(sec)
                continue;
            end
            prn = line(4:6);
            for i = 1:nsat
                if (lgds(i, :) == prn)
                    time = sec + 1;
                    Data1(i, time) = str2num(line(41:59));
                    break;
                end
            end
        end
    end
    fclose(fid1);
end

% read clk product from igs, can read more than one file by more filename
function clk = read_igs_clk(sysid, interval, varargin)
    %   eg:clk = read_igs_clk('G',300,'C:\clk_2019100');
    nfile = nargin -2;
    nsat = 32;
    switch sysid
        case 'G'
            nsat = 32;
        case 'R'
            nsat = 24;
        case 'E'
            nsat = 30;
        case 'C'
            nsat = 16;
        case 'J'
            nsat = 2;
    end
    clk = nan * ones(nfile * 86400 / interval, nsat);

    for ifile = 1:1:nfile
        fid = fopen(varargin{ifile}, 'r');
        while ~feof(fid)
            line = fgetl(fid);
            if findstr(line, 'END OF HEADER')
                continue;
            end
            if findstr(line(1:2), 'AS')
                if findstr(line(4:4), sysid)
                    sat_id = str2num(line(5:6));
                    if sat_id <= nsat
                        hour = str2num(line(20:21));
                        minute = str2num(line(23:24));
                        second = str2num(line(26:34));

                        epoch = (hour * 3600 + minute * 60 + second) / interval;
                        if (abs(epoch - round(epoch)) < 0.01)
                            sclk = str2num(line(39:59));
                            epoch = (ifile - 1) * 86400 / interval + (hour * 3600 + minute * 60 + second) / interval + 1;
                            clk(epoch, sat_id) = sclk;
                        end
                    end
                end
            end
        end
        fclose(fid);
    end
    clk = clk';
end

% read ifcb product from GREAT
function ifcb = read_ifcb(sysid, interval, filename)
    %   eg: ifcb=read_ifcb_file(filename,'G')
    sats = [];
    if sysid == 'G'
        sats = [1, 3, 6, 8, 9, 10, 24, 25, 26, 27, 30, 32];
    end
    if sysid == 'J'
        sats = [1, 2, 3, 7];
    end
    if sysid == 'E'
        sats = [1:5, 7, 8, 9, 11:15, 18, 19, 21, 24:27, 30, 31, 33, 36];
    end
    if sysid == 'C'
        sats = [1:14];
    end
    len = 86400 / interval;
    nsats = length(sats);
    ifcb = nan * ones(nsats, len); %24*60*2=2880

    fid = fopen(filename, 'r');

    mjd = 0;
    sod = 0;
    while ~feof(fid)
        line = fgetl(fid);
        if findstr(line, '%')
            continue;
        end
        if findstr(line, 'EPOCH-TIME')
            mjd = str2num(line(15:19));
            sod = str2num(line(20:29)); %second of day
            continue;
        end
        which_col = sod / interval;
        if which_col ~= fix(which_col)
            continue;
        end
        which_col = which_col + 1;
        if (line(1:1) ~= ' ')
            continue;
        end
        sat_id = str2num(line(3:4));
        which_row = find(sats == sat_id);
        ifcb(which_row, which_col) = str2num(line(16:22));
    end
    fclose(fid);
end

% draw clkdif
function pgcf = draw_clkdif(Dif, interval, sysid)
    % return a handle of a picture
    switch sysid
        case 'G'
            lgds = ['G01'; 'G02'; 'G03'; 'G04'; 'G05'; 'G06'; 'G07'; 'G08'; 'G09'; 'G10'; 'G11'; 'G12';
                'G13'; 'G14'; 'G15'; 'G16'; 'G17'; 'G18'; 'G19'; 'G20'; 'G21'; 'G22'; 'G23'; 'G24';
                'G25'; 'G26'; 'G27'; 'G28'; 'G29'; 'G30'; 'G31'; 'G32'];
        case 'E'
            lgds = ['E01'; 'E02'; 'E03'; 'E04'; 'E05'; 'E07'; 'E08'; 'E09'; 'E11'; 'E12'; 'E13'; 'E14'; 'E15'; 'E18'; 'E19'; 'E21'; 'E24'; 'E25'; 'E26'; 'E27'; 'E30'; 'E31'; 'E33'; 'E36'];
        case 'C'
            lgds = ['C06'; 'C07'; 'C08'; 'C09'; 'C10'; 'C11'; 'C12'; 'C13'; 'C14'; 'C16'];
    end
    nsat = size(lgds, 1);
    len = 86400 / interval; %12*24=288  5min
    nfigures = ceil(nsat / 4); %4 sat=1 fig
    figure;
    set(gcf, 'position', [0 0 50 + 50 + 70 * nfigures 50 + 70 * 4])
    for i = 1:1:nsat
        x = 50 + 70 * (i - nfigures * (ceil(i / nfigures) - 1) - 1);
        y = 50 + 70 * (4 - ceil(i / nfigures));
        subplot(4, nfigures, i);
        %     plot((1:len),(Dif(i,:))','.b','MarkerSize', 4);
        plot((1:len), smooth((Dif(i, :))', 'rlowess'), '.b', 'MarkerSize', 4);
        set(gca, 'YLim', [-0.25, 0.25]);
        set(gca, 'YTick', -0.2:0.1:0.2);
        set(gca, 'XLim', [1, len]);
        set(gca, 'XTick', 0:len / 12:len);
        if i == 1 || i == 1 + nfigures || i == 1 + 2 * nfigures || i == 1 + 3 * nfigures || i == 1 + 4 * nfigures
            set(gca, 'yticklabel', {'-0.2', '-0.1', '0', '0.1', '0.2'});
        else
            set(gca, 'yticklabel', []);
        end
        if i == 4
            ylabel('CLKDIF — PIFCB (m)', 'FontSize', 10);
        end
        if i >= nfigures * 3 + 1 && i <= 4 * nfigures
            %xlabel('Time (Hour)','FontSize',10);
            if i == nsat
                set(gca, 'xticklabel', {'0', ' ', ' ', ' ', '8', ' ', ' ', ' ', '16', ' ', ' ', ' ', '24'});
            else
                set(gca, 'xticklabel', {'0', ' ', ' ', ' ', '8', ' ', ' ', ' ', '16', ' ', ' ', ' ', ''});
            end
        else
            set(gca, 'xticklabel', []);
        end
        if i == 3 * nfigures + 2
            xlabel('Time (Hour)', 'FontSize', 10);
        end
        set(gca, 'FontSize', 10, 'FontName', 'Arial');
        legend_tick = lgds(i, :);
        legend(legend_tick, 'Orientation', 'horizontal');
        legend boxoff;
        hold on; grid on;
        set(gca, 'position', [x / (70 * nfigures + 50 + 50) y / (70 * 4 + 50) 70 / (50 + 50 + 70 * nfigures) 70 / (50 + 70 * 4)]);
    end
    pgcf = gcf;
end

% draw PIFCB
function pgcf = draw_ifcb(Dif, interval, sysid)
    % pgcf=draw_ifcb(Dif,300,'G');
    % return a handle of a picture
    switch sysid
        case 'G'
            lgds = ['G01'; 'G03'; 'G06'; 'G08'; 'G09'; 'G10'; 'G24'; 'G25'; 'G26'; 'G27'; 'G30'; 'G32'];
        case 'E'
            lgds = ['E01'; 'E02'; 'E03'; 'E04'; 'E05'; 'E07'; 'E08'; 'E09'; 'E11'; 'E12'; 'E13'; 'E14'; 'E15'; 'E18'; 'E19'; 'E21'; 'E24'; 'E25'; 'E26'; 'E27'; 'E30'; 'E31'; 'E33'; 'E36'];
        case 'C'
            lgds = ['C06'; 'C07'; 'C08'; 'C09'; 'C10'; 'C11'; 'C12'; 'C13'; 'C14'; 'C16'];
    end
    nsat = size(lgds, 1);
    len = 86400 / interval; %12*24=288  5min
    nfigures = ceil(nsat / 4); %4 sat=1 fig
    figure;
    set(gcf, 'position', [0 0 50 + 50 + 70 * nfigures 50 + 70 * 4])
    for i = 1:1:nsat
        x = 50 + 70 * (i - nfigures * (ceil(i / nfigures) - 1) - 1);
        y = 50 + 70 * (4 - ceil(i / nfigures));
        subplot(4, nfigures, i);
        %     plot((1:len),(Dif(i,:))','.b','MarkerSize', 4);
        plot((1:len), smooth((Dif(i, :))', 'rlowess'), '.b', 'MarkerSize', 4);
        set(gca, 'YLim', [-0.25, 0.25]);
        set(gca, 'YTick', -0.2:0.1:0.2);
        set(gca, 'XLim', [1, len]);
        set(gca, 'XTick', 0:len / 12:len);
        if i == 1 || i == 1 + nfigures || i == 1 + 2 * nfigures || i == 1 + 3 * nfigures || i == 1 + 4 * nfigures
            set(gca, 'yticklabel', {'-0.2', '-0.1', '0', '0.1', '0.2'});
        else
            set(gca, 'yticklabel', []);
        end
        if i == 1
            ylabel('PIFCB (m)', 'FontSize', 10);
        end
        if i >= nfigures * 3 + 1 && i <= 4 * nfigures
            xlabel('Time (Hour)', 'FontSize', 10);
            if i == nsat
                set(gca, 'xticklabel', {'0', ' ', '4', ' ', '8', ' ', '12', ' ', '16', ' ', '20', ' ', '24'});
            else
                set(gca, 'xticklabel', {'0', ' ', '4', ' ', '8', ' ', '12', ' ', '16', ' ', '20', ' ', ''});
            end
        else
            set(gca, 'xticklabel', []);
        end
        set(gca, 'FontSize', 10, 'FontName', 'Arial');
        legend_tick = lgds(i, :);
        legend(legend_tick, 'Orientation', 'horizontal');
        legend boxoff;
        hold on; grid on;
        set(gca, 'position', [x / (70 * nfigures + 50 + 50) y / (70 * 4 + 50) 70 / (50 + 50 + 70 * nfigures) 70 / (50 + 70 * 4)]);
        pgcf = gcf;
    end
end

% all hangs minus the first hang
function ans = zhongxinh(mat)
    [a, b] = size(mat);
    for i = 2:a
        mat(i, :) = mat(i, :) - mat(1, :);
    end
    mat(1, :) = mat(1, :) - mat(1, :);
    ans = mat;
end

% all lies minus the first lie
function ans = zhongxinl(mat)
    [a, b] = size(mat);
    for i = 2:b
        mat(:, i) = mat(:, i) - mat(:, 1);
    end
    mat(:, 1) = mat(:, 1) - mat(:, 1);
    ans = mat;
end
