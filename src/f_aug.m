% ------------------------------------------------------------------
% FileName:         f_aug.m
% Author:           hlgou
% Version:          V 1.0
% Created:          2021-11-05
% Description:      read, process, draw aug file
% History:
%          2021-11-05   hlgou       :Create the file
%          2021-xx-xx   xxxxxxxx    :Do some modified
% ------------------------------------------------------------------


% clc;clear;
wdir = 'C:\Users\OHanlon\Documents\programData\QQ\308046421\FileRecv\wanghao\';
f1 = [wdir, 'R293-GREC-251.aug'];
f2 = [wdir, 'GEOP-GEC.aug'];
f3 = 'C:\Users\OHanlon\Desktop\plotData\aug\282\test.txt';
% [Gdata1,Edata1,Cdata1,number1]=aug2mat(f1);
% [Gdata2,Edata2,Cdata2,number2]=aug2mat(f2);

Gdata = minsAug(Gdata1, Gdata2);
Edata = minsAug(Edata1, Edata2);
Cdata = minsAug(Cdata1, Cdata2);
Gdatam = Gdata; Edatam = Edata; Cdatam = Cdata;
Gdatam = minsRef(Gdata, 3);
Edatam = minsRef(Edata, 3);

drawAug(Gdatam, 'G');
drawAug(Edatam, 'E');
drawAug(Cdatam, 'C');
% drawSatNum(number1);
% drawSatNum(number2);


% get GEC AUG data from file.
%
% > @param[in] f_aug:       file name
% return:
% < @param[out] Gdata:      GPS aug data
% < @param[out] Edata:      GAL aug data
% < @param[out] Cdata:      BDS aug data
% < @param[out] number:     the number of available satellites in 3 System
function [Gdata, Edata, Cdata, number] = aug2mat(f_aug)
    ft = f_time;
    Gdata = []; Edata = []; Cdata = []; number = [];
    Gtmp = nan * zeros(33, 5);
    Etmp = nan * zeros(37, 5);
    Ctmp = nan * zeros(62, 5);

    fid = fopen(f_aug, 'r');
    tag = 0; num = 0;
    while ~feof(fid)
        lin = fgetl(fid);
        if lin(1) == ">"
            if (tag > 0)
                Gdata(:, :, tag) = Gtmp;
                Edata(:, :, tag) = Etmp;
                Cdata(:, :, tag) = Ctmp;
                number(tag, 3) = num;
            end
            Gtmp = nan * zeros(33, 5); Gtmp(2:33, 1) = [1:32];
            Etmp = nan * zeros(37, 5); Etmp(2:37, 1) = [1:36];
            Ctmp = nan * zeros(62, 5); Ctmp(2:62, 1) = [1:61];
            num = 0;
            tag = tag + 1;
            tmp = strsplit(strtrim(lin), {' '});
            year = str2num(tmp{2});
            mon = str2num(tmp{3});
            day = str2num(tmp{4});
            hour = str2num(tmp{5});
            min = str2num(tmp{6});
            sec = str2num(tmp{7});
            %         if length(tmp)>=8
            %             num=str2num(tmp{8});
            %         end
            [year, doy] = ft.ymd2doy(year, mon, day);
            sod = ft.hms2sod(hour, min, sec);
            Gtmp(1, 1) = doy; Gtmp(1, 2) = sod;
            Etmp(1, 1) = doy; Etmp(1, 2) = sod;
            Ctmp(1, 1) = doy; Ctmp(1, 2) = sod;
            number = [number; doy, sod, 0];
            continue;
        end
        if tag == 0 || length(lin) < 5 || isempty(strtrim(lin(4:end)))
            continue;
        end
        p1 = nan; p2 = nan; l1 = nan; l2 = nan;
        sys = lin(1);
        id = str2num(lin(2:3));
        p1 = str2num(lin(6:16));
        l1 = str2num(lin(18:27));
        if length(lin) > 30 || ~isempty(strtrim(lin(30:end)))
            p2 = str2num(lin(30:40));
            l2 = str2num(lin(42:51));
        end
        if abs(p1) > 100 p1 = nan; end
        if abs(l1) > 100 l1 = nan; end
        if abs(p2) > 100 p2 = nan; end
        if abs(l2) > 100 l2 = nan; end
        eval([sys, 'tmp(id+1,2)=p1;']);
        eval([sys, 'tmp(id+1,3)=l1;']);
        eval([sys, 'tmp(id+1,4)=p2;']);
        eval([sys, 'tmp(id+1,5)=l2;']);
        num = num + 1;
    end
    fclose(fid);
    Gdata = rmNanSats(Gdata); Edata = rmNanSats(Edata); Cdata = rmNanSats(Cdata);
    Gdata(:, :, end) = []; Edata(:, :, end) = []; Cdata(:, :, end) = []; number(end, :) = [];
end

% remove the sat whose data is all-nan
function Data = rmNanSats(data)
    nsats = size(data, 1);
    Data = data;
    ind = [];
    for i = 1:nsats
        tmp = data(i, 2:5, :);
        if all(isnan(tmp))
            ind = [ind; i];
        end
    end
    Data(ind, :, :) = [];
end

% plot the number of satellite figure
function pgcf = drawSatNum(number)
    figure;
    time = number(:, 2) / 3600;
    num = number(:, 3);
    doy = number(1, 1);
    plot(time, num, '.', 'color', 'b', 'MarkerSize', 20);
    title(['DOY:', num2str(doy, '%02d')]);
    ylabel('The number of sats', 'FontSize', 10);
    xlabel('GPS Time(HOD)', 'FontSize', 10);
    set(gca, 'YLim', [0, max(num) + 2]);
    pgcf = gcf;
end

% plot the AUG figure of the specific system
function drawAug(Data, sys)
    ut = f_util;

    figure;
    nsat = size(Data, 1);
    nepo = size(Data, 3);
    sats = reshape(Data(2:nsat, 1, 1), 1, nsat - 1);
    lgds = ut.num2sat_char(sys, sats);
    time = reshape(Data(1, 2, :), nepo, 1);
    time = time ./ 3600;
    c = colormap(lines(nsat));
    for i = 2:5
        subplot(2, 2, i - 1);
        for j = 2:nsat
            dat = reshape(Data(j, i, :), nepo, 1);
            plot(time, dat, '*', 'color', c(j, :), 'MarkerSize', 4);
            hold on;
        end
        switch i
            case 2
                ytext = ['P1(m)'];
                xtext = '';
                legend(lgds, 'FontSize', 8, 'Orientation', 'horizontal', 'Box', 'off', 'Location', 'Best', 'NumColumns', 6);
            case 3
                ytext = ['L1(m)'];
                xtext = '';
            case 4
                ytext = ['P2(m)'];
                xtext = 'GPS Time(HOD)';
            case 5
                ytext = ['L2(m)'];
                xtext = 'GPS Time(HOD)';
        end
        ylabel(ytext, 'FontSize', 10);
        xlabel(xtext, 'FontSize', 10);
    end
end

% Data = Data1 - Data2, the common time and sats
function Data = minsAug(Data1, Data2)
    nsat1 = size(Data1, 1); sats1 = reshape(Data1(:, 1, 1), 1, nsat1);
    nsat2 = size(Data2, 1); sats2 = reshape(Data2(:, 1, 1), 1, nsat2);
    csats = intersect(sats1, sats2);
    nepo1 = size(Data1, 3); time1 = reshape(Data1(1, 2, :), nepo1, 1);
    nepo2 = size(Data2, 3); time2 = reshape(Data2(1, 2, :), nepo2, 1);
    ctime = intersect(time1, time2);
    Data = nan * zeros(length(csats) + 1, 5, length(ctime));
    [isornot, sind1] = ismember(csats, sats1); sind1 = sort(sind1);
    [isornot, sind2] = ismember(csats, sats2); sind2 = sort(sind2);
    [isornot, eind1] = ismember(ctime, time1);
    [isornot, eind2] = ismember(ctime, time2);
    dat1 = Data1(sind1, :, eind1);
    dat2 = Data2(sind2, :, eind2);
    Data = dat1 - dat2;
    Data(1, :, :) = dat1(1, :, :);
    Data(:, 1, :) = dat1(:, 1, :);
end

% all the satellites' AUG - the ref's AUG
function Data = minsRef(Data1, ref)
    Data = Data1;
    nsat = size(Data1, 1);
    for i = 2:nsat
        Data(i, 2, :) = Data(i, 2, :) - Data1(ref, 2, :);
        Data(i, 3, :) = Data(i, 3, :) - Data1(ref, 3, :);
        Data(i, 4, :) = Data(i, 4, :) - Data1(ref, 4, :);
        Data(i, 5, :) = Data(i, 5, :) - Data1(ref, 5, :);
    end
end
