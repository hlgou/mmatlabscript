% ------------------------------------------------------------------
% FileName:         f_site.m
% Author:           hlgou
% Version:          V 1.0
% Created:          2021-10-09
% Description:      plot site distribution
% History:
%          2021-10-15   hlgou       :Create the file
%          2021-10-19   hlgou       :Add some function
%          2021-xx-xx   xxxxxxxx    :Do some modified
% ------------------------------------------------------------------

clc; clear;
f1='C:\Users\OHanlon\Downloads\site_list_081.txt';
f2='C:\Users\OHanlon\Downloads\blh.txt';
sta = txt2sta(f2);
pgcf = drawSta1(sta);
sites = getSitelist(f1);

% cell mat 2 str mat
function Data = cellm2strm(cemat)
    n = length(cemat);
    data = [];
    for i = 1:n
        data = [data; cemat{i}];
    end
    Data = data;
end

% get sitelist
function sites = getSitelist(f_site)
    sites = textread(f_site,'%s');
    sites = cellm2strm(sites);
end

% get sta1 info from sta by sitelist 3
function sta1 = getSta(sitelist, sta)
    sta1 = [];
    stalist = get_fdata(sta, 'name');
    stalist = cellstr(stalist(:, 1:3));
    n = size(sitelist, 1);
    sit = cellstr(sitelist(:, 1:3));
    for i = 1:n
        [is, wh] = ismember(sit(i), stalist);
        if is
            tmp = sta(wh);
            tmp.name = sitelist(i, :);
            sta1 = [sta1; tmp];
        end
    end
end

% get sta1 info from sta by sitelist 4 best
function sta1 = getSta1(sitelist, sta)
    sta1 = [];
    stalist = {sta.name};
    n = size(sitelist, 1);
    sit = cellstr(sitelist);
    for i = 1:n
        [is, wh] = ismember(sit(i), stalist);
        if is
            tmp = sta(wh);
            tmp.name = sitelist(i, :);
            sta1 = [sta1; tmp];
        end
    end
end

% get sta1 info from sta by sitelist 3
function STA1 = getSta2(sitelist, sta)
    sta1 = [];
    stalist = get_fdata(sta, 'name');
    stalist = cellstr(stalist(:, 1:3));
    n = size(sitelist, 1);
    sit = cellstr(sitelist(:, 1:3));
    for i = 1:n
        [is, wh] = ismember(sit(i), stalist);
        if is
            tmp = sta(wh);
            tmp.name = sitelist(i, :);
            sta1 = [sta1; tmp];
        end
    end
    STA1 = sta1;
end

% draw from sta struct
function pgcf = drawSta(sta, sta1)
    n = length(sta);
    figure; %plot
    m_proj('Equidistant Cylindrical', 'lon', [-185, 185], 'lat', [-90, 90]);
    m_coast('patch', [.7 .7 .7], 'edgecolor', 'none');
    m_grid('box', 'fancy', 'xtick', -180:60:180, 'ytick', -90:30:90);
    for i = 1:n
        nam = sta(i).name;
        lon = sta(i).lon;
        lat = sta(i).lat;
        m_line(lon, lat, 'marker', '.', 'markersize', 20, 'color', 'b', 'MarkerFaceColor', 'b');
        %m_text(lon,lat,nam,'vertical','top','fontsize',8);
    end
    n = length(sta1);
    for i = 1:n
        nam = sta1(i).name;
        lon = sta1(i).lon;
        lat = sta1(i).lat;
        m_line(lon, lat, 'marker', 'V', 'markersize', 5, 'color', 'r', 'MarkerFaceColor', 'r');
        %m_text(lon,lat,nam,'vertical','top','fontsize',8);
    end
    pgcf = gcf;
end

% draw from sta struct
function pgcf = drawSta1(sta)
    n = length(sta);
    figure; %plot
    m_proj('Equidistant Cylindrical', 'lon', [-185, 185], 'lat', [-90, 90]);
    m_coast('patch', [.7 .7 .7], 'edgecolor', 'none');
    m_grid('box', 'fancy', 'xtick', -180:60:180, 'ytick', -90:30:90);
    for i = 1:n
        nam = sta(i).name;
        lon = sta(i).lon;
        lat = sta(i).lat;
        m_line(lon, lat, 'marker', '.', 'markersize', 20, 'color', [0 0 1], 'MarkerFaceColor', 'b');
        m_text(lon,lat,nam,'vertical','top','fontsize',8);
    end
    pgcf = gcf;
end

% draw from sta struct col and mark
function pgcf = drawSta3(sta, col, mark, size)
    n = length(sta);
    figure; %plot
    m_proj('Equidistant Cylindrical', 'lon', [-180, 180], 'lat', [-90, 90]);
    m_coast('patch', [.7 .7 .7], 'edgecolor', 'none');
    m_grid('box', 'fancy', 'xtick', -180:60:180, 'ytick', -90:30:90);
    for i = 1:n
        nam = sta(i).name;
        lon = sta(i).lon;
        lat = sta(i).lat;
        m_line(lon, lat, 'marker', mark, 'markersize', size, 'color', col, 'MarkerFaceColor', col);
        m_text(lon, lat, nam, 'vertical', 'top', 'color', 'r', 'fontsize', 4);
    end
    pgcf = gcf;
end

% draw from sta struct col and mark append
function pgcf = drawSta2(pgcf, sta, col, mark, size)
    n = length(sta);
    gcf = pgcf;
    get(gca);
    for i = 1:n
        nam = sta(i).name;
        lon = sta(i).lon;
        lat = sta(i).lat;
        m_line(lon, lat, 'marker', mark, 'markersize', size, 'color', col, 'MarkerFaceColor', col);
        %m_text(lon,lat,nam,'vertical','top','fontsize',8);
    end
    pgcf = gcf;
end

% get the field value from sta
function data = get_fdata(sta, field)
    data = [];
    af = fieldnames(sta);
    n = length(af);
    for i = 1:n
        if mstrcmp(af{i}, field) == 0
            break;
        end
    end
    if (mstrcmp(af{i}, field) ~= 0)
        disp('----error----');
        disp(['The filed ''', field, ''' is not in sta!']);
        disp('------end----');
        return;
    end
    m = length(sta);
    for i = 1:m
        data = eval(['[data;sta(i).', field, '];']);
    end
end

% to strcmp two strings -1 0 1
function p = mstrcmp(str1, str2)
    k = min(length(str1), length(str2));
    for n = 1:k %Compare the top k
        if (str1(n) > str2(n))
            p = 1; break;
        elseif (str1(n) == str2(n))
            p = 0;
        else p = -1; break;
        end
    end
    if (p == 0)
        if (length(str1) > length(str2)) %The first k bits are equal, but str1 is longer
            p = 1;
        elseif (length(str1) == length(str2))
            p = 0;
        else p = -1;
        end
    end
end

% get station info from txt to a struct
function sta = txt2sta(IGSsitefile)
    fid3 = fopen(IGSsitefile);
    i = 1;
    while ~feof(fid3)
        str = fgetl(fid3);
        tmp = strsplit(str,{' ',','});
        sta(i).name = tmp{1};
        sta(i).lon = str2double(tmp{2});
        sta(i).lat = str2double(tmp{3});
        sta(i).alt = str2double(tmp{4});
        i = i + 1;
    end
    fclose(fid3);
end

% write sta to txt
function sta2txt(sta, fname)
    fid2 = fopen(fname, 'w');
    n = length(sta);
    for i = 1:n
        name = sta(i).name;
        lon = sta(i).lon;
        lat = sta(i).lat;
        alt = sta(i).alt;
        fprintf(fid2, '%4s %16.8f %16.8f %8.1f\r\n', name, lon, lat, alt);
    end
    fclose(fid2);
end

% get site lat/lon from IGS.txt --> another txt
function getLon2txt(IGSsitefile, sitelist, sitefile)
    sitelist = sort(sitelist);
    fid1 = fopen(IGSsitefile);
    fid2 = fopen(sitefile, 'w');
    t = 1; % sitelist count
    while ~feof(fid1)
        station = fgetl(fid1);
        name = station(1:4);
        while 1
            if t <= size(sitelist, 1) && all(name == sitelist{t})
                fprintf(fid2, [station, '\n']);
                t = t + 1;
            end
            if t > size(sitelist, 1)
                break;
            end
            if mstrcmp(name, sitelist{t}) == -1
                break;
            else
                t = t + 1;
            end
        end
        if t > size(sitelist, 1)
            break;
        end
    end
    fclose(fid1);
    fclose(fid2); %get the station information from IGS.txt with site_list
end
