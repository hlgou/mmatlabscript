% ------------------------------------------------------------------
% FileName:         f_util.m
% Author:           hlgou
% Version:          V 1.0
% Created:          2021-10-09
% Description:      some little useful function
% History:
%          2021-10-09   hlgou       :Create the file
%          2021-xx-xx   xxxxxxxx    :Do some modified
% ------------------------------------------------------------------

% function handle
function A = f_util
    A.get_stu = @get_stu;
    A.get_fdata = @get_fdata;
    A.site2mat = @site2mat;
    A.getSite_dir = @getSite_dir;
    A.findseries = @findseries;
    A.num2sat_char = @num2sat_char;
    A.num2sat_cell = @num2sat_cell;
    A.cellm2strm = @cellm2strm;
    A.cell2num = @cell2num;
    A.ave_weight = @ave_weight;
    A.judge_jump = @judge_jump;
    A.mstrcmp = @mstrcmp;
    A.find_sat_char = @find_sat_char;
    A.find_sat_cell = @find_sat_cell;
end

% get struct from clkdif file
function stu = get_stu(file)
    fid = fopen(file, 'r');
    str = fgetl(fid);
    S = regexp(str, '\s+', 'split'); %field
    n = length(S);
    j = 1;
    while ~feof(fid)
        str = fgetl(fid);
        str = strtrim(str); %rm the blankSpace on the beg and end
        if str(1) == '-'
            continue;
        end
        if str(1:3) == 'EOF'
            break;
        end
        tmp = regexp(str, '\s+', 'split');
        for i = 1:n
            eval(['stu(j).', S{i}, '=tmp{i};']);
        end
        j = j + 1;
    end
    fclose(fid);
end

% get all the value of the field from a struct
function data = get_fdata(sta, field)
    data = [];
    af = fieldnames(sta);
    n = length(af);
    for i = 1:n
        if mstrcmp(af{i}, field) == 0
            break;
        end
    end
    if (mstrcmp(af{i}, field) ~= 0)
        disp('----error----');
        disp(['The filed ''', field, ''' is not in sta!']);
        disp('------end----');
        return;
    end
    m = length(sta);
    for i = 1:m
        data = eval(['[data;sta(i).', field, '];']);
    end
end

% get sitelist from txt
function Data1 = site2mat(file)
    Data1 = [];
    fid1 = fopen(file, 'r');
    while ~feof(fid1)
        line = fgetl(fid1);
        Data1 = [Data1; upper(line(2:5))];
    end
    fclose(fid1);
end

% get sitelist from dir, the result is a n*4 chars-mat
function site = getSite_dir(enudir)
    dirOutput = dir(fullfile(enudir));
    plyName = {dirOutput.name}; %get a cell mat
    plyName = plyName(3:end); %rm . and ..
    n = length(plyName);
    sitelist = "";
    for i = 1:n
        fname = plyName{i};
        sitelist = strcat(sitelist, " ", fname(1:4));
    end
    sitelist = unique(regexp(sitelist, '\s', 'split'));
    sitelist = sitelist(2:end); %rm the first ""
    %string2char
    nsite = length(sitelist);
    site = [];
    for i = 1:nsite
        site = [site; char(sitelist(i))];
    end
end

% find continuous m(10) epoch and return, or return 0
%
% > @param[in] x:           the vector
% > @param[in] pos:         the start position where finding begin
% > @param[in] m:           the number of need to be continuous
% return:
% < @param[out] firstvalue:    the first position which meet the condition
function firstvalue = findseries(x, pos, m)
    n = length(x);
    while 1
        if (pos + m > n)
            firstvalue = 0;
            break;
        end
        if (x(pos) + m == x(pos + m))
            firstvalue = x(pos);
            break;
        end
        pos = pos + 1;
    end
end

% convert sat_num list to sat_char list, `satlist=num2sat_char('G',num)`
function satlist = num2sat_char(sys, num)
    satlist = [];
    for i = num
        satlist = [satlist; sys, num2str(i, '%02d')];
    end
end

% convert numList to cellSatList
function satlist = num2sat_cell(sys, num)
    satlist = {};
    for i = num
        satlist = [satlist, cellstr([sys, num2str(i, '%02d')])];
    end
end

% convert cell_mat to str_mat
function data = cellm2strm(cemat)
    n = length(cemat);
    data = [];
    for i = 1:n
        data = [data; cemat{i}];
    end
end

% convert cell_char_mat to double_mat
function num = cell2num(data)
    [a, b] = size(data);
    num = zeros(a, b);
    for i = 1:a
        for j = 1:b
            num(i, j) = str2double(data{i, j});
        end
    end
end

% compute average with weight
% the first clounm of data is the value, the second is the the weight
function ave = ave_weight(data)
    if isempty(data)
        ave = 0;
        return
    end
    std = data(:, 1);
    weight = data(:, 2);
    weight = weight ./ sum(weight);
    ave = sum(std .* weight);
end

% judge whether a new data can be contained in a group, data < 3*std(group)
function whe = judge_jump(group, data)
    me = nanmean(group);
    st = nanstd(group);
    whe = false;
    if (abs(data - me) > 3 * st)
        whe = true;
    end
end

% compare two strings, 1> 1; 1= 0; 1< -1
function p = mstrcmp(str1, str2)
    k = min(length(str1), length(str2));
    for n = 1:k%Compare the top k
        if (str1(n) > str2(n))
            p = 1; break;
        elseif (str1(n) == str2(n))
            p = 0;
        else p = -1; break;
        end
    end
    if (p == 0)
        if (length(str1) > length(str2))%The first k bits are equal, but str1 is longer
            p = 1;
        elseif (length(str1) == length(str2))
            p = 0;
        else p = -1;
        end
    end
end

% find the sat index from a charSatList
function prn = find_sat_char(satlist, sat)
    n = size(satlist, 1);
    prn = 0;
    for i = 1:n
        if mstrcmp(satlist(i, :), sat) == 0
            prn = i;
            break;
        end
    end
end

% find the sat index from a cellSatList
function prn = find_sat_cell(satlist, sat)
    n = length(satlist);
    prn = 0;
    for i = 1:n
        if mstrcmp(satlist{i}, sat) == 0
            prn = i;
            break;
        end
    end
end
