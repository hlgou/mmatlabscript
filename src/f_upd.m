% ------------------------------------------------------------------
% FileName:         f_upd.m
% Author:           hlgou
% Version:          V 1.0
% Created:          2021-10-09
% Description:      read, process, draw upd-files
% History:
%          2021-10-09   hlgou       :Create the file
%          2021-xx-xx   xxxxxxxx    :Do some modified
% ------------------------------------------------------------------

function A = f_upd
    A.read_nl = @read_nl;
    A.read_wl = @read_wl;
    A.read_allNl = @read_allNl;
    A.minusRef = @minusRef;
    A.draw_nl = @draw_nl;
    A.draw_wl = @draw_wl;
    A.draw_nlStd = @draw_nlStd;
    A.draw_nlStd1 = @draw_nlStd1;
    A.draw_nlStd2 = @draw_nlStd2;
    A.draw_upd_all = @draw_upd_all;
    A.compute_std = @compute_std;
    A.compute_sstd = @compute_sstd;
    A.merge_nl = @merge_nl;
end

% get single system raw_Epoch_upd data from file
function upd = read_nl(file, sys)
    sats = [];
    switch sys
        case 'G'
            sats = 1:32;
        case 'E'
            sats = 1:36;
        case 'C'
            sats = 1:61;
        otherwise
            disp('Error::read_nl ## Unknow system ID!');
            return;
    end
    n = size(sats, 2);
    upd = sats;
    fid = fopen(file, 'r');
    upd = [0 0, upd]; % the first cloum is mjd, the second is sod
    row = 1;
    while ~feof(fid)
        line = fgetl(fid);
        if findstr(line, '%')
            continue;
        end
        if findstr(line, 'EPOCH-TIME')
            mjd = str2num(line(15:19));
            sod = str2num(line(20:29));
            tmp = [mjd, sod, zeros(1, n) * nan];
            upd = [upd; tmp];
            row = row + 1;
            continue;
        end
        if (line(1:1) ~= ' ' || line(2) ~= sys)
            continue;
        end
        sat_id = str2num(line(3:4));
        clo = find(upd(1, :) == sat_id);
        upd(row, clo) = str2num(line(16:22));
    end
    fclose(fid);
    index = []; % rm sats list
    for i = 1:n + 2
        if isnan(upd(2:end, i))
            index = [index i];
        end
    end
    upd(:, index) = [];
end

% get single system raw_Daily_upd data from file
function upd = read_wl(file, sys)
    sats = [];
    switch sys
        case 'G'
            sats = 1:32;
        case 'E'
            sats = 1:36;
        case 'C'
            sats = 1:61;
        otherwise
            disp('Error::read_wl ## Unknow system ID!');
            return;
    end
    n = size(sats, 2);
    upd = sats;
    fid = fopen(file, 'r');
    upd = [upd; nan * zeros(1, n)];
    while ~feof(fid)
        line = fgetl(fid);
        if line(1:1) ~= ' ' || line(2) ~= sys
            continue;
        end
        sat_id = str2num(line(3:4));
        clo = find(upd(1, :) == sat_id);
        upd(2, clo) = str2num(line(16:22));
    end
    fclose(fid);
    index = []; % rm sats list
    for i = 1:n
        if isnan(upd(2:end, i))
            index = [index i];
        end
    end
    upd(:, index) = [];
end

% get all system raw_Epoch_upd data from file
function [Gupd, Eupd, Cupd] = read_allNl(file)
    sys = 'GEC';
    Gsats = 1:32;
    Esats = 1:36;
    Csats = 1:61;
    nG = size(Gsats, 2);
    nE = size(Esats, 2);
    nC = size(Csats, 2);
    Gupd = [0 0, Gsats];
    Eupd = [0 0, Esats];
    Cupd = [0 0, Csats];
    fid = fopen(file, 'r');
    row = 1;
    while ~feof(fid)
        line = fgetl(fid);
        if findstr(line, '%')
            continue;
        end
        if findstr(line, 'EPOCH-TIME')
            mjd = str2num(line(15:19));
            sod = str2num(line(20:29));
            Gtmp = [mjd, sod, zeros(1, nG) * nan];
            Etmp = [mjd, sod, zeros(1, nE) * nan];
            Ctmp = [mjd, sod, zeros(1, nC) * nan];
            Gupd = [Gupd; Gtmp];
            Eupd = [Eupd; Etmp];
            Cupd = [Cupd; Ctmp];
            row = row + 1;
            continue;
        end
        if (line(1:1) ~= ' ')
            continue;
        end
        sysid = line(2);
        sat_id = str2num(line(3:4));
        switch sysid
            case 'G'
                clo = find(Gupd(1, :) == sat_id);
                Gupd(row, clo) = str2num(line(16:22));
            case 'E'
                clo = find(Eupd(1, :) == sat_id);
                Eupd(row, clo) = str2num(line(16:22));
            case 'C'
                clo = find(Cupd(1, :) == sat_id);
                Cupd(row, clo) = str2num(line(16:22));
        end
    end
    fclose(fid);

    index = []; % rm sats list
    for i = 1:nG + 2
        if isnan(Gupd(2:end, i))
            index = [index i];
        end
    end
    Gupd(:, index) = [];
    index = [];
    for i = 1:nE + 2
        if isnan(Eupd(2:end, i))
            index = [index i];
        end
    end
    Eupd(:, index) = [];
    index = [];
    for i = 1:nC + 2
        if isnan(Cupd(2:end, i))
            index = [index i];
        end
    end
    Cupd(:, index) = [];
end

% all satellite upd value minus the reference satellite upd value
function upd = minusRef(upd_epoch, sat)
    nsats = size(upd_epoch, 2) - 2;
    for i = 1:nsats
        if i ~= sat
            upd_epoch(2:end, 2 + i) = upd_epoch(2:end, 2 + i) - upd_epoch(2:end, 2 + sat);
        end
    end
    upd_epoch(2:end, 2 + sat) = upd_epoch(2:end, 2 + sat) - upd_epoch(2:end, 2 + sat);
    % Adjust jump caused by subtracting reference satellite
    [row, col] = size(upd_epoch);
    for i = 2:row
        for j = 3:col
            if abs(upd_epoch(i, j) - upd_epoch(2, j)) > 0.6
                jump = round(upd_epoch(i, j) - upd_epoch(2, j));
                upd_epoch(i, j) = upd_epoch(i, j) - jump;
            end
        end
        upd = upd_epoch;
    end
end

% draw epoch_upd
function pgcf = draw_nl(upd, sys, updmode)
    ut = f_util;
    tm = f_time;
    sats = upd(1, 3:end);
    nsat = size(sats, 2);
    satlist = ut.num2sat_char(sys, sats);
    mjd = upd(2, 1);
    sod = upd(2, 2);
    [week0, how0] = tm.sod2how(mjd, sod);
    nepo = size(upd, 1) - 1;
    t = [];
    for i = 1:nepo
        mjd = upd(i + 1, 1);
        sod = upd(i + 1, 2);
        [week, how] = tm.sod2how(mjd, sod);
        if week > week0
            how = how + (week - week0) * 24 * 7;
        end
        t = [t; week0 how];
    end
    %figure;
    c = colormap(lines(nsat));
    p = [];
    for i = 1:nsat
        p1 = plot(t(:, 2), upd(2:end, i + 2), '*', 'color', c(i, :), 'MarkerSize', 4);
        p = [p p1];
        hold on;
    end
    legend(p, satlist, 'FontSize', 8, 'Orientation', 'horizontal', 'Box', 'off', 'Location', 'Best', 'NumColumns', 10);
    title([num2str(week0), ' - ', sys, ' - ', updmode]);
    ylabel([updmode, ' UPD(Cycles)'], 'FontSize', 10);
    xlabel('GPS Time (Hour of week)');
    set(gca, 'YLim', [-1, 1]);
    pgcf = gcf;
end

% draw daily_upd
function pgcf = draw_wl(upd, sys, tit)
    ut = f_util;
    sats = upd(1, :);
    nsats = size(upd, 2);
    satc = ut.num2sat_char(sys, sats);
    figure;
    bar(upd(2, :), 'group', 'FaceColor', 'b');
    set(gca, 'XTick', 1:nsats, 'xticklabel', '');
    set(gca, 'YLim', [-1, 1]);
    set(gca, 'YTick', -1:0.5:1);
    ylabel('WL UPD(Cycles)', 'FontSize', 10);
    set(gca, 'FontSize', 10, 'FontName', 'Arial');
    title(tit);
    xt = get(gca, 'XTick');
    yt = get(gca, 'YTick');
    xtextp = xt;
    ytextp = (-0.01 * (yt(end) - yt(1)) + yt(1)) * ones(1, length(xt));
    text(xtextp, ytextp, satc, 'HorizontalAlignment', 'right', 'rotation', 30, 'fontsize', 10);
    pgcf = gcf;
end

% draw upd std
function pgcf = draw_nlStd(ustd, sys, updmode)
    ut = f_util;
    nsats = size(ustd, 2);
    num = ustd(1, :);
    std_value = ustd(2, :);
    lgds = ut.num2sat_char(sys, num);
    bar(std_value, 'group', 'FaceColor', 'b');
    grid on;
    hold on;
    set(gca, 'XLim', [0, nsats + 1]);
    set(gca, 'YLim', [0, 0.1]);
    set(gca, 'XTick', 1:1:nsats);
    set(gca, 'xticklabel', lgds);
    set(gca, 'YTick', 0:0.02:0.1);
    % Set the position, angle and font size of the new display label
    xtb = get(gca, 'XTickLabel');
    xt = get(gca, 'XTick');
    yt = get(gca, 'YTick');
    xtextp = xt;
    ytextp = -0.3 * yt(2) * ones(1, length(xt));
    text(xtextp, ytextp, xtb, 'HorizontalAlignment', 'right', 'rotation', 30, 'fontsize', 10);
    % Hide the original label
    set(gca, 'xticklabel', '');
    ylabel('UPD STD(Cycles)', 'FontSize', 10);
    set(gca, 'FontSize', 10, 'FontName', 'Arial');
    title([sys, '-STD-', updmode]);
    pgcf = gcf;
end

% draw upd std sub of draw_updStd2
function pgcf = draw_nlStd1(ustd, sys)
    ut = f_util;
    nsats = size(ustd, 2);
    num = ustd(1, :);
    std_value = ustd(2:end, :);
    std_value = std_value';
    lgds = ut.num2sat_char(sys, num);
    figure
    bar(std_value, 'group');
    grid on;
    hold on;
    set(gca, 'XLim', [0, nsats + 1]);
    set(gca, 'YLim', [0, 0.1]);
    set(gca, 'XTick', 1:1:nsats);
    set(gca, 'xticklabel', lgds);
    set(gca, 'YTick', 0:0.02:0.1);
    % Set the position, angle and font size of the new display label
    xtb = get(gca, 'XTickLabel');
    xt = get(gca, 'XTick');
    yt = get(gca, 'YTick');
    xtextp = xt;
    ytextp = -0.3 * yt(2) * ones(1, length(xt));
    text(xtextp, ytextp, xtb, 'HorizontalAlignment', 'right', 'rotation', 30, 'fontsize', 10);
    % Hide the original label
    set(gca, 'xticklabel', '');
    ylabel('UPD STD(Cycles)', 'FontSize', 10);
    set(gca, 'FontSize', 10, 'FontName', 'Arial');
    legend(['WL'; 'NL']);
    title([sys, '-STD']);
    pgcf = gcf;
end

% draw upd std 2 subfig
function pgcf = draw_nlStd2(ustdwl, ustdnl, sys)
    figure
    subplot(2, 1, 1);
    draw_nlStd(ustdwl, sys, 'WL');
    subplot(2, 1, 2);
    draw_nlStd(ustdnl, sys, 'NL');
    pgcf = gcf;
end

% draw wl and nl epoch_upd
function pgcf = draw_upd_all(wl, nl, sys)
    figure
    subplot(2, 1, 1);
    draw_nl(wl, sys, 'WL');
    subplot(2, 1, 2);
    draw_nl(nl, sys, 'NL');
    pgcf = gcf;
end

% compute upd std
function ustd = compute_std(upd)
    sats = upd(1, 3:end);
    n = size(sats, 2);
    ustd = nan * zeros(2, n);
    ustd(1, :) = sats;
    for i = 1:n
        data = upd(2:end, i + 2);
        ustd(2, i) = compute_sstd(data, 0.05);
    end
end

% compute a satellite std
function sstd = compute_sstd(data, jump)
    ut = f_util;
    n = size(data, 1);
    record = [];
    data_tmp = [];
    tag = 0;
    for i = 1:n
        if (isnan(data(i)))
            num = size(data_tmp, 1);
            if (num >= 20)
                record = [record; nanstd(data_tmp), num];
            end
            data_tmp = [];
            tag = 0;
            continue;
        end
        num = size(data_tmp, 1);
        if (num ~= 0)
            % abs(data_tmp(num)-data(i))>jump
            % judge_jump(data_tmp,data(i))
            if (abs(data_tmp(num) - data(i)) > jump)
                if (num >= 20)
                    record = [record; nanstd(data_tmp), num];
                end
                data_tmp = [];
                tag = 0;
                continue;
            end
        end
        data_tmp = [data_tmp; data(i)];
        tag = 1;
    end
    if tag == 1
        num = size(data_tmp, 1);
        if (num >= 20)
            record = [record; nanstd(data_tmp), num];
        end
        data_tmp = [];
        tag = 0;
    end
    sstd = ut.ave_weight(record);
end

% Merge files into another file(NL UPD)
function merge_nl(f1, f2, f)
    fidin1 = fopen(f1, 'r');
    fidin2 = fopen(f2, 'r');
    fidin3 = fopen(f, 'w');
    while ~feof(fidin1)
        tline = fgetl(fidin1);
        if strcmp(tline(1), '%') == 1
            %fprintf(fidin3,'%s\n',tline);
            continue;
        end
        if strcmp(tline, 'EOF') == 1
            continue;
        end
        if strcmp(tline(2:6), 'EPOCH') == 0
            fprintf(fidin3, '%s\n', tline);
            continue;
        else
            while ~feof(fidin2)
                tline2 = fgetl(fidin2);
                if strcmp(tline2, 'EOF') == 1
                    continue;
                end
                if (strcmp(tline2(2:6), 'EPOCH') ~= 1)
                    fprintf(fidin3, '%s\n', tline2);
                    continue;
                elseif strcmp(tline2(2:6), 'EPOCH') == 1
                    break;
                end
            end
            fprintf(fidin3, '%s\n', tline);
        end
    end
    while ~feof(fidin2)
        tline2 = fgetl(fidin2);
        fprintf(fidin3, '%s\n', tline2);
    end
    %fprintf(fidin3,'%s\n','EOF');
    fclose(fidin1);
    fclose(fidin2);
    fclose(fidin3);
end
