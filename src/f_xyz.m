% ------------------------------------------------------------------
% FileName:         f_xyz.m
% Author:           hlgou
% Version:          V 1.0
% Created:          2021-10-09
% Description:      read, process, draw xyz-files such as enu, flt
% History:
%          2021-10-09   hlgou       :Create the file
%          2021-xx-xx   xxxxxxxx    :Do some modified
% ------------------------------------------------------------------

% function handle
function A = f_xyz
    A.enu2mat = @enu2mat;
    A.enuflt2mat = @enuflt2mat;
    A.ratio2mat = @ratio2mat;
    A.draw_enu = @draw_enu;
    A.getConTime = @getConTime;
    A.findseries = @findseries;
    A.getTTFF = @getTTFF;
    A.getTTFF1 = @getTTFF1;
    A.getTTFF2 = @getTTFF2;
    A.get_meanENU = @get_meanENU;
    A.subLast = @subLast;
end

% get enu mat from txt
function Data1 = enu2mat(file)
    % Data1 n*4 is the enumat
    Data1 = [];
    fid1 = fopen(file, 'r');
    while ~feof(fid1)
        line = fgetl(fid1);
        if (line(2:4) ~= 'RMS')
            Data1 = [Data1; str2num(line)];
        else
            rms = [inf, str2num(line(7:19)), str2num(line(20:32)), str2num(line(33:end))];
            Data1 = [Data1; rms];
        end
    end
    if (abs(Data1(end - 1, 4)) > 1)
        Data1 = Data1 - [0, mean(Data1(1:end - 1, 2:4))];
        Data1(end, 2:4) = std(Data1(1:end - 1, 2:4));
    end
    fclose(fid1);
end

% get re-xyz from ENU_flt file
function Data1 = enuflt2mat(file)
    % get eunmat by reading enufile
    % Data1 n*4 is the enumat
    Data1 = [];
    fid1 = fopen(file, 'r');
    while ~feof(fid1)
        line = fgetl(fid1);
        if (line(1:1) == '%')
            continue;
        else
            rms = [str2num(line(52:63)), str2num(line(64:78)), str2num(line(79:93)), str2num(line(94:108))];
            Data1 = [Data1; rms];
        end
    end
    if (abs(Data1(end - 1, 4)) > 1)
        Data1 = Data1 - [0, Data1(end - 1, 2:4)];
    end
    fclose(fid1);
end

% get ratio mat from txt
function Data = ratio2mat(fname)
    [ta, ra] = textread(fname, '%f%f', 'headerlines', 1);
    Data = [ta, ra];
end

% draw enu
function pgcf = draw_enu(Data, tag)
    % ex: draw_enu(Data,[tag;title])
    crl_ = ['rbgcmyk']; %color
    n = size(Data, 3); %number of data type
    m = size(Data, 1); %number of epoch
    interval = Data(2, 1, 1) - Data(1, 1, 1);
    num_m = 3600 / interval; %the num of epoch in an hour
    num_h = m / num_m; %the num of hour
    f_hour = fix(Data(1, 1, 1) / 3600);
    if (n + 1) ~= size(tag, 1)
        disp('-------error-------');
        disp('the div of Data and tag is not match!');
        disp('len(tag) must be len(Data)+1');
        disp('tag=[tg1;ta2;...;title]');
        disp('--------end-------');
        return
    end
    crl = crl_(1:n);
    crl = fliplr(crl);
    figure;
    subplot(3, 1, 1);
    r = [];
    t = [];
    for i = 1:n
        color = crl(i);
        Data1 = Data(:, :, i);
        r1 = plot(1:m, Data1(:, 2), ['.', color], 'MarkerSize', 5); hold on;
        r = [r r1];
        t = [t tag(i, :)];
    end
    set(gca, 'YLim', [-0.2, 0.2]);
    set(gca, 'YTick', -0.2:0.1:0.2);
    legend(r, t, 'Orientation', 'horizon', 'Box', 'off');
    title(tag{n + 1});
    xx = m;
    set(gca, 'XLim', [0, xx]);
    set(gca, 'XTick', 0:num_m:xx);
    set(gca, 'xticklabel', {});
    ylabel('East(m)', 'FontSize', 10);
    grid on;
    subplot(3, 1, 2);
    for i = 1:n
        color = crl(i);
        Data1 = Data(:, :, i);
        plot(1:m, Data1(:, 3), ['.', color], 'MarkerSize', 5); hold on;
    end
    set(gca, 'YLim', [-0.2, 0.2]);
    set(gca, 'YTick', -0.2:0.1:0.2);
    set(gca, 'XLim', [0, xx]);
    set(gca, 'XTick', 0:num_m:xx);
    set(gca, 'xticklabel', {});
    ylabel('North(m)', 'FontSize', 10);
    grid on;
    subplot(3, 1, 3);
    for i = 1:n
        color = crl(i);
        Data1 = Data(:, :, i);
        plot(1:m, Data1(:, 4), ['.', color], 'MarkerSize', 5); hold on;
    end
    set(gca, 'XLim', [0, xx]);
    set(gca, 'XTick', 0:num_m:xx);
    set(gca, 'YLim', [-0.4, 0.4]);
    set(gca, 'YTick', -0.4:0.2:0.4);
    ylabel('Up(m)', 'FontSize', 10);
    x = f_hour:(f_hour + num_h);
    x1 = sprintfc('%g', x); %
    xlabel('GPS Time(hour)', 'FontSize', 10);
    set(gca, 'xticklabel', x1);
    set(gca, 'FontSize', 10, 'FontName', 'Arial'); grid on;
    hold off;
    pgcf = gcf;
end

% get the convergence time from enumat
function ctime = getConTime(Data)
    % convergence time is the time of EN < 5cm
    x = Data(:, 2);
    y = Data(:, 3);
    time = Data(:, 1);
    firstepo = time(1);
    N1 = find(abs(x) < 0.05);
    N2 = find(abs(y) < 0.05);
    k = 1;
    t = 1;
    while 1
        epos = findseries(N1, k, 10);
        npos = findseries(N2, t, 10);
        if (epos == 0 || npos == 0)
            ctime = 9999;
            break;
        end
        if (epos == npos)
            ctime = (time(epos) - firstepo) / 60;
            break;
        end
        if (epos > npos)
            t = t + 1;
        else
            k = k + 1;
        end
    end
end

% find continuous m(10) epoch and return, or return 0
%
% > @param[in] x:           the vector
% > @param[in] pos:         the start position where finding begin
% > @param[in] m:           the number of need to be continuous
% return: 
% < @param[out] firstvalue:    the first position which meet the condition
function firstvalue = findseries(x, pos, m)
    n = length(x);
    while 1
        if (pos + m > n)
            firstvalue = 0;
            break;
        end
        if (x(pos) + m == x(pos + m))
            firstvalue = x(pos);
            break;
        end
        pos = pos + 1;
    end
end

% get the TTFF from ratiomat: fixed in continuous 10 epoch
function TTFF = getTTFF(Data1, ratio)
    % fixed in continuous 10 epoch
    if isempty(ratio)
        TTFF = inf;
        return;
    end
    time = Data1(:, 1);
    ta = ratio(:, 1);
    firstepo = time(1);
    interval = time(2) - time(1);
    epos = findseries(ta, 1, 5);
    if epos == 0
        TTFF = inf;
    else
        TTFF = (time(epos) - firstepo) / 60;
    end
end

% get the TTFF from ratiomat: fixed and AR<F in continuous 10 epoch
% Data1 is float data; Data2 is fixed data
function TTFF = getTTFF1(Data1, Data2, ratio)
    % fixed and AR<F in continuous 10 epoch
    if isempty(ratio)
        TTFF = inf;
        return;
    end
    x1 = Data1(:, 2); x2 = Data2(:, 2); xx = find(x1 >= x2);
    y1 = Data1(:, 3); y2 = Data2(:, 3); yy = find(y1 >= y2);
    z1 = Data1(:, 4); z2 = Data2(:, 4); zz = find(z1 >= z2);
    xy = intersect(xx, yy); xyz = intersect(xy, zz); %enu of AR<=F
    time = Data1(:, 1);
    ta = ratio(:, 1);
    tt = intersect(xyz, ta); %fixed and AR<=F
    firstepo = time(1);
    epos = findseries(tt, 1, 5); %3 epochs
    if epos == 0
        TTFF = inf;
    else
        TTFF = (time(epos) - firstepo) / 60;
    end
end

% get the TTFF from ratiomat: fixed and AR<0.05 in continuous 5 epoch
function TTFF = getTTFF2(Data1, ratio)
    % fixed and AR<0.05 in continuous 5 epoch
    if isempty(ratio)
        TTFF = nan;
        return;
    end
    ep = ratio(:, 1);
    x1 = Data1(:, 2);
    y1 = Data1(:, 3);
    z1 = Data1(:, 4);
    xx = find(abs(x1) <= 0.05); yy = find(abs(y1) <= 0.05);
    ta = intersect(xx, yy); %fixed and AR<=F
    tt = intersect(ta, ep);
    time = Data1(:, 1);
    firstepo = time(1);
    epos = findseries(tt, 1, 5); %3 epochs
    if epos == 0
        TTFF = nan;
    else
        TTFF = (time(epos) - firstepo) / 60;
    end
end

% get meanENU in 5~10 mins
function enu = get_meanENU(ENU)
    be = 10; en = 20;
    data = ENU(be:en, :);
    tmp = nanmean(data);
    tmp = rms(ENU(en:end, :));
    enu = tmp(2:4);
end

% all of the front data sub the last-epoch data
function Data = subLast(Data1)
    Data = Data1 - [0, Data1(end - 1, 2:4)];
end
