% ------------------------------------------------------------------
% FileName:         f_plot.m
% Author:           hlgou
% Version:          V 1.0
% Created:          2021-11-05
% Description:      some small functions of plot
% History:
%          2021-11-05   hlgou       :Create the file
%          2021-xx-xx   xxxxxxxx    :Do some modified
% ------------------------------------------------------------------

f1 = 'C:\Users\OHanlon\Downloads\crd.txt';
plot_crd(f1);

% get the crd from the file
function [name, xyz] = read_crd(f_xyz)
    fid = fopen(f_xyz);
    name = [];
    xyz = [];
    while ~feof(fid)
        str = fgetl(fid);
        tmp = strsplit(str, {' ', ',', '	'});
        name = [name; tmp{1}];
        x = str2double(tmp{2});
        y = str2double(tmp{3});
        z = str2double(tmp{4});
        xyz = [xyz; x, y, z];
    end
    fclose(fid);
end

% plot the crd
function plot_crd(f_xyz)
    [name, xyz] = read_crd(f_xyz);
    X = xyz(:, 1);
    Y = xyz(:, 2);
    xmin = min(X);
    xmax = max(X);
    ymin = min(Y);
    ymax = max(Y);

    xmin0 = xmin - (xmax - xmin) * 0.05;
    xmax0 = xmax + (xmax - xmin) * 0.05;
    ymin0 = ymin - (ymax - ymin) * 0.05;
    ymax0 = ymax + (ymax - ymin) * 0.05;
    figure;
    scatter(X, Y, 'filled');
    set(gca, 'YLim', [ymin0, ymax0]);
    set(gca, 'XLim', [xmin0, xmax0]);
    axis equal
    n = size(name, 1);
    for i = 1:n
        x = X(i);
        y = Y(i);
        text(x, y, name(i, :), 'color', 'k');
    end
end
