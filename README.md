# 一、绘图有关
## 1、坐标误差时间序列图
### 1. FLT
```matlab
xy = f_xyz;
ut = f_util;
wdir = 'C:\Users\OHanlon\Desktop\plotData\xyz\';
sdir = 'C:\Users\OHanlon\Desktop\plotData\fig\';
f1 = [wdir, 'flt\ABMF_2021187_GE_2_IF.flt'];
f2 = [wdir, 'flt\ABMF_2021187_GE_2_IF_new.flt'];
Data1 = xy.enuflt2mat(f1);
Data2 = xy.enuflt2mat(f2);
Da1 = xy.subLast(Data1(1:350, :));
Da2 = xy.subLast(Data2(1:350, :));
Data(:, :, 1) = Da1;
Data(:, :, 2) = Da2;
site = 'ABMF';
tag = {'float'; 'fixed'; site};
pgcf = xy.draw_enu(Data, tag);
set(pgcf, 'Position', [0, 0, 1304, 690], 'color', 'w'); % set fig size
sf = [sdir, 'flt.png'];
saveas(pgcf, sf); % save fig

```
![FLT](images/FLT.png)
### 2. ENU
```matlab
xy = f_xyz;
ut = f_util;
wdir = 'C:\Users\OHanlon\Desktop\plotData\xyz\';
sdir = 'C:\Users\OHanlon\Desktop\plotData\fig\';
f1 = [wdir, 'enu\ABMF_2021187_GE_2_IF_F.enu'];
f2 = [wdir, 'enu\ABMF_2021187_GE_2_IF_AR.enu'];
Data1 = xy.enu2mat(f1);
Data2 = xy.enu2mat(f2);
Da1 = xy.subLast(Data1(1:350, :));
Da2 = xy.subLast(Data2(1:350, :));
Data(:, :, 1) = Da1;
Data(:, :, 2) = Da2;
site = 'ABMF';
tag = {'float'; 'fixed'; site};
pgcf = xy.draw_enu(Data, tag);
set(pgcf, 'Position', [0, 0, 1304, 690], 'color', 'w'); % set fig size
sf = [sdir, 'ENU.png'];
saveas(pgcf, sf); % save fig
```
![ENU](images/ENU.png)

## 2、UPD 
窄巷UPD
```matlab
ud = f_upd;
wdir = 'C:\Users\OHanlon\Desktop\plotData\upd\';
sdir = 'C:\Users\OHanlon\Desktop\plotData\fig\';
f1 = [wdir, 'upd_nl_2021212_GEC_IF_com'];
sys = 'G'; updmode = 'NL';
upd = ud.read_nl(f1, sys);
figure;
pgcf = ud.draw_nl(upd, sys, updmode);
set(pgcf, 'Position', [0, 0, 1304, 690], 'color', 'w'); % set fig size
sf = [sdir, 'NLUPD.png'];
saveas(pgcf, sf); % save fig
ustd = ud.compute_std(upd);

figure;
pgcf = ud.draw_nlStd(ustd, sys, updmode);
set(pgcf, 'Position', [0, 0, 1304, 690], 'color', 'w'); % set fig size
sf = [sdir, 'NLUPDStd.png'];
saveas(pgcf, sf); % save fig
```
![NL UPD](images/NLUPD.png)
![NL UPD STD](images/NLUPDStd.png)

宽巷UPD
```matlab
ud = f_upd;
wdir = 'C:\Users\OHanlon\Desktop\plotData\upd\';
sdir = 'C:\Users\OHanlon\Desktop\plotData\fig\';
f1 = [wdir, 'upd_wl_2021212_GEC_IF_com'];
sysid = 'G';
ifcb1 = ud.read_wl(f1, 'G');
tit = '2021212 WL';
pgcf = ud.draw_wl(ifcb1, sysid, tit);
set(pgcf, 'Position', [0, 0, 1304, 690], 'color', 'w'); % set fig size
sf = [sdir, 'WLUPD.png'];
saveas(pgcf, sf); % save fig
```
![WL UPD](images/WLUPD.png)

所有UPD
```matlab
ud = f_upd;
wdir = 'C:\Users\OHanlon\Desktop\plotData\upd\';
sdir = 'C:\Users\OHanlon\Desktop\plotData\fig\';
f1 = [wdir, 'upd_nl_2021212_GEC_IF_com'];
f2 = [wdir, 'upd_nl_2021212_GEC_UC_com'];
[Gupd1, Eupd1, Cupd1] = ud.read_allNl(f1);
[Gupd2, Eupd2, Cupd2] = ud.read_allNl(f2);
for sys = ['G']
    eval(['wl=', sys, 'upd1;']);
    eval(['nl=', sys, 'upd2;']);
    pgcf = ud.draw_upd_all(wl, nl, sys);
    set(pgcf, 'Position', [0, 0, 1304, 690], 'color', 'w'); % set fig size
    sf = [sdir, 'allUPD.png'];
    saveas(pgcf, sf); % save fig
    eval([sys, 'ustd1=ud.compute_std(wl);']);
    eval([sys, 'ustd2=ud.compute_std(nl);']);
    eval(['ustd1=', sys, 'ustd1;']);
    eval(['ustd2=', sys, 'ustd2;']);
    pgcf = ud.draw_nlStd2(ustd1, ustd2, sys);
    set(pgcf, 'Position', [0, 0, 1304, 690], 'color', 'w'); % set fig size
    sf = [sdir, 'allUPDStd.png'];
    saveas(pgcf, sf); % save fig
end
```
![ALL UPD](images/allUPD.png)
![ALL UPD STD](images/allUPDStd.png)

## 3、CLK & PIFCB
PIFCB
```matlab
ck = f_clk;
wdir = 'C:\Users\OHanlon\Desktop\plotData\clk\';
sdir = 'C:\Users\OHanlon\Desktop\plotData\fig\';
f1 = [wdir, 'ifcb_2020097'];
sysid = 'G'; interval = 30;
ifcb1 = ck.read_ifcb(sysid, interval, f1);
pgcf = ck.draw_ifcb(ifcb1, interval, sysid);
set(pgcf, 'Position', [0, 0, 1304, 690], 'color', 'w'); % set fig size
sf = [sdir, 'PIFCB.png'];
saveas(pgcf, sf); % save fig
```
![PIFCB](images/PIFCB.png)
12频 - 13频 钟差
```matlab
ck = f_clk;
wdir = 'C:\Users\OHanlon\Desktop\plotData\clk\';
sdir = 'C:\Users\OHanlon\Desktop\plotData\fig\';
f1 = [wdir, 'clk_2020097_GE_f12'];
f2 = [wdir, 'clk_2020097_GE_f13'];
sysid = 'G'; interval = 30;
Data1 = ck.read_clk13(sysid, interval, f1);
Data2 = ck.read_clk13(sysid, interval, f2);
Dif = (Data2 - Data1) * 299792458; %ns -> m
Dif = Dif - (nanmean(Dif'))'; %cloum
Dif = Dif - (nanmean(Dif)); %row
pgcf = ck.draw_ifcb(Dif, interval, sysid);
set(pgcf, 'Position', [0, 0, 1304, 690], 'color', 'w'); % set fig size
sf = [sdir, 'CLKDIF.png'];
saveas(pgcf, sf); % save fig
```
![CLKDIF](images/CLKDIF.png)
与机构产品钟差之差
```matlab
ck = f_clk;
wdir = 'C:\Users\OHanlon\Desktop\plotData\clk\';
sdir = 'C:\Users\OHanlon\Desktop\plotData\fig\';
f1 = [wdir, 'com21001.clk'];
f2 = [wdir, 'clk_2020097_GE_f12'];
sysid = 'G'; interval = 30;
Data1 = ck.read_igs_clk(sysid, interval, f1);
Data2 = ck.read_clk12(sysid, interval, f2);
Dif = (Data2 - Data1) * 299792458; %ns -> m
Dif = Dif - (nanmean(Dif'))'; %cloum
Dif = Dif - (nanmean(Dif)); %row
Dif = ck.zhongxinh(Dif); %set the first sat as reference sat
pgcf = ck.draw_clkdif(Dif, interval, sysid);
set(pgcf, 'Position', [0, 0, 1304, 690], 'color', 'w'); % set fig size
sf = [sdir, 'CLKDIF1.png'];
saveas(pgcf, sf); % save fig
```
![CLKDIF1](images/CLKDIF1.png)
## 4、测站分布图
```matlab
IGSsitefile = 'C:\gwork\navigation\matlab\plot\mat\sta.mat';
f1 = 'C:\Users\OHanlon\Desktop\plotData\site\site_list_test';
f2 = 'C:\Users\OHanlon\Desktop\plotData\site\site_list_GREC';
load(IGSsitefile);
s1 = textread(f1, '%s'); sitelist1 = cellm2strm(s1);
s2 = textread(f2, '%s'); sitelist2 = cellm2strm(s2);
sta1 = getSta(sitelist1, sta); sta2 = getSta(sitelist2, sta);
%drawSta1(sta2);
drawSta(sta2, sta1);
```
这里面用到了事先存好的测站位置数据`sta`，绘制出来的图如下所示：


# 二、实用功能

